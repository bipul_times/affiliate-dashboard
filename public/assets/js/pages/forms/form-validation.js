var tempLoader ="<div id='_tmploader' style='z-index:12;top:0;bottom:0;left:0;right:0;position:fixed;width:100%;height:100%;background:rgba(0,0,0,0.5);display: flex;vertical-align: middle;align-items: center;align-content: center; text-align: center;justify-content: center;'><div class='loader'><div class='preloader'> <div class='spinner-layer pl-red'> <div class='circle-clipper left'>  <div class='circle'></div> </div><div class='circle-clipper right'> <div class='circle'></div></div></div> </div></div></div>";
$(function () {
     $('#payment_form_validation').validate({
        rules: {
            
            'account_number' :{
                required: true,
                numericValue : true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'bank_name' :{
                required: true,
                normalizer: function (value) {
                    return $.trim(value);
                }
               // alphabeticValue : true
            },
            'branch_name' :{
                required: true,
                normalizer: function (value) {
                    return $.trim(value);
                }
                //alphaNumericValue : true
            },
            'ifsc_code' :{
                required: true,
                alphaNumericValue : true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'pancard_number' :{
                required: true,
                validPanCard : true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'pancard_image' :{
                //required: true,
                fileType: 'jpeg,jpg,png,pdf',
                maxFileSize: 5120000,//5 MB 100 KB or 100000 Byte                
            },
            'aadhar_number' :{
                required: true,
                numericValue : true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'aadhar_image' :{
                //required: true,
                fileType: 'jpeg,jpg,png,pdf',
                maxFileSize: 5120000,//5 MB 100 KB or 100000 Byte                
            },
            'cheque_image' :{
               // required: true,
                fileType: 'jpeg,jpg,png,pdf',
                maxFileSize: 5120000,//100 KB or 100000 Byte                
            },
            'gst_number' :{
                required: true,
                alphaNumericValue : true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'gst_certificate' :{
               // required: true,
                fileType: 'jpeg,jpg,png,pdf',
                maxFileSize: 5120000,//5 MB 100 KB or 100000 Byte               
            },
            'gst_undertaking' :{
             //   required: true,
                fileType: 'pdf',
                maxFileSize: 5120000,//5 MB 100 KB or 100000 Byte                
            },
            'self_attest_copy' :{
              //  required: true,
                fileType: 'jpeg,jpg,png,pdf',
                maxFileSize: 5120000,//5 MB 100 KB or 100000 Byte                
            },
            'name_declaration_image' :{
              //  required: true,
                fileType: 'jpeg,jpg,png,pdf',
                maxFileSize: 5120000,//5 MB 100 KB or 100000 Byte                
            },
            'pin_code' :{
              PinCode:true,
              normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'address' :{
             addressValidation: true,
              normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'accept_contract_aggrement':{
                required: true
            },
            
            
        },
        messages: {
            gst_undertaking: {
                fileType: 'Please enter a value with a valid file(pdf)',
            },
            accept_contract_aggrement: {
                required: 'Please accept general terms and conditions',
            }
        },
       submitHandler: function () {
           $('.sbtbtn').prop('disabled', true);
            var formData = new FormData($("#payment_form_validation").get(0));
            $('body').append(tempLoader);
            $.ajax({
                type: "POST",
                url: $("#payment_form_validation").attr("action"),
                data: formData,
                processData: false,
                contentType: false,
              //  async: false,
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                 success: function (responseData) {
                    $('.sbtbtn').prop('disabled', false);
                    console.log(responseData.response.message);
                    if (responseData.response.status == true) {
                        $('body').find('#_tmploader').remove();
                        //console.log(responseData.response.message);
                        swal({
                            title: "Good job!",
                            text: responseData.response.message,
                            type: "success",
                            closeOnConfirm: false,
                        }, function () {
                            location.reload(true)
                        });
                    } else if (responseData.response.status == false) {
                         $('body').find('#_tmploader').remove();
                        var li = '';
                        $.each(responseData.response.message, function (index, val) {
                            //console.log(val)
                            li += '<li>' + val + '</li>';
                        });
                        var successMsg = '<div class="alert alert-danger alert-block "><button type="button" class="close" data-dismiss="alert">×</button><strong><ul>' + li + '</ul></strong></div>';
                        $(".successMsgDiv").html(successMsg);
                        $(".successMsgDiv").show();
                        $("html, body").animate({scrollTop: 0}, "slow");

                    }

                 }
             });
             return false; 
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        
    });
    
    
    $('#form_validation').validate({
        rules: {
            'checkbox': {
                required: true
            },
            'gender': {
                required: true
            },
            
            
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });
    $('#form_assignment').validate({
        rules: {
            'checkbox': {
                required: true
            },
            'gender': {
                required: true
            },
            'image': {
                fileType: 'jpeg,jpg,png',
                maxFileSize: 5120000,//5 MB 100 KB or 100000 Byte
            },
            'title': {
                //required: true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'seo_title': {
                alphaNumericValue: true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
           
            'synopsis': {
                //alphaNumericValue: true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'content': {
                required: true,
                validateTinyMceData:true,
            },
            'video': {
                //checkVideoUploadBtn:true,
                fileType: 'mp4',
                maxFileSize: 51200000,// 50 MB 100 KB or 100000 Byte //2000
            },            
            
            
        },
        
        messages: {
            video: {
                maxFileSize: "File size must not exceed 50MB and file type should be mp4",
                fileType: 'File type should be mp4 of max size 50 MB',
            },
            image: {
                fileType: 'File type should be jpeg,jpg,png of max size 5 MB',
            }
        },
        submitHandler: function () {
            tinyMCE.triggerSave(true, true);
            
             
            $(".successMsgDiv").hide();
            $('.sbtbtn').prop('disabled', true);
            var keyword = $('input[name="keyword"]').val();
            keyword = $.trim(keyword);
            //console.log(keyword,'IKHWAN');
            var theEditor = tinymce.activeEditor;
            var content = theEditor.getContent();
            content = $.trim(content);
            var contentwordCount =0
            if(content){
                var contentStr = content;
                contentStr = contentStr.toString();
                contentStr = contentStr.replace(/<br\s*[\/]?>/gi, ' ');
                
                var temporalDivElement = document.createElement("div");
                temporalDivElement.innerHTML = contentStr;
                contentStr =temporalDivElement.textContent || temporalDivElement.innerText || ""
                contentStr = contentStr.replace(/\n/, " ");
                contentStr = contentStr.replace(/\s{2,}/g, ' ');
                contentStr = contentStr.replace(/&nbsp;/g, "");
                contentStr = contentStr.replace(/\r /," ");        
                
                contentStr = contentStr.trim();
                if (contentStr.match(/\s+/g)) {
                    contentwordCount = contentStr.match(/\s+/g).length;
                    contentwordCount = contentwordCount + 1;
                } else {
                    contentwordCount = 1;
                }
                
            }            
           // var wordCount = theEditor.plugins.wordcount.getCount();
            $('#tinymce_word_count').val(contentwordCount);
            var isContentError = false;
            var iskeywordError = false;
            var isVideoValError =false;
            var iskeywordLengthError = false;
            
            /*if (keyword === "" || keyword === null) {
                var label = '<label id="keywork-error" for="keyword" class="error CWError" >keyword field is required</label>';
                $("#keyword").parent().parent().append(label);
                $("#keyword").focus();
               iskeywordError = true;
            }else{
                var keywordRes = keyword.split(",");
                //console.log(keyword,'IKHWAN 111');
                $.each(keywordRes, function (index, keyVal) {
                    //console.log(keyVal,'IKHWAN---keyVal');
                    //console.log(keyVal.length,'IKHWAN---len');
                    if (keyVal.length > 50) {                        
                        $("#keyword-helptext").addClass('col-red');
                        $("#keywordtags").focus();
                        iskeywordLengthError =true;;
                    }
                });
            }*/
            //console.log(keyword,'IKHWAN---111');
            
            if (content === "" || content === null) {                
                var label = '<label id="tinymce-error"  class="error CWError" >Content field is required</label>';
                $("#tinymce").parent().parent().append(label);
                 $("#tinymce").focus();
               isContentError = true;
            }
            
            //var videoVal = '';
            if ($('#video').length) {
                var videoVal = document.getElementById('video').files[0];
                console.log(videoVal, 'video');
                if (typeof videoVal != 'undefined') {
                    console.log(videoVal);
                    $("#video-error").remove();
                    var label = '<label id="video-error" for="video" class="error CWError" >Please click on upload button to upload video</label>';
                    $("#video").parent().parent().append(label);
                    $("#video").focus();
                    isVideoValError = true;
                }
            }
            
            
            
            if(isContentError===true || iskeywordError===true || isVideoValError===true || iskeywordLengthError===true){
                $('.sbtbtn').prop('disabled', false);
                return false;
            }else{
                var formData = new FormData($("#form_assignment").get(0));
                if($('#temp_img_error').val()==1){
                     $('.sbtbtn').prop('disabled', false);
                    return false;
                    
                }else{
                    
                    $('body').append(tempLoader);
                    $.ajax({
                    url: tLoginObj.home_url + '/saveassignment',
                    data: formData,
                    type: "POST",
                    processData: false,
                    contentType: false,
              //  async: false,
                    headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                    success: function (responseData) {
                        $('.sbtbtn').prop('disabled', false);
                        //document.getElementsByClassName("sbtbtn").disabled = false;
                        $('.sbtbtn').prop('disabled', false);
                        if (responseData.response.status == true) {
                            $('body').find('#_tmploader').remove();
                            console.log(responseData.response.message);
                            swal({
                                title: "Good job!",
                                text: responseData.response.message,
                                type: "success",
                                closeOnConfirm: false,
                            }, function () {
                                location.reload(true)
                            });
                        } else if (responseData.response.status == false) {
                            $('body').find('#_tmploader').remove();
                            var li = '';
                            $.each(responseData.response.message, function (index, val) {
                                console.log(val)
                                li += '<li>' + val + '</li>';
                            });
                            var successMsg = '<div class="alert alert-danger alert-block "><button type="button" class="close" data-dismiss="alert">×</button><strong><ul>' + li + '</ul></strong></div>';
                            $(".successMsgDiv").html(successMsg);
                            $(".successMsgDiv").show();
                            $("html, body").animate({scrollTop: 0}, "slow");

                        }



                    },
                    error: function (responseData) {
                        $('.sbtbtn').prop('disabled', false);
                        $('body').find('#_tmploader').remove();
                        //console.log(JSON.stringify(e));


                    }
                });
                }
                
                
            }
            return false;
            
            
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
            //$('#btnSubmit').show(); 
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
            //$('#btnSubmit').hide(); 
        }
    });
    
    $('#review_payment_form_validation').validate({
        rules: {
            'checkbox': {
                required: true
            },
            'gender': {
                required: true
            },
            'writer_type': {
                required: true
            },
            'stringer_amount':{
                numericValue:true
            },
            'intern_amount':{
                numericValue:true
                
            },
            'freelancer_perword_amount':{
                priceWithDecimal:true
            },
            'freelancer_perarticle_amount':{
                numericValue:true
                
            },
            'min_article':{
                numericValueWithOutZero:true
                
            },
            'max_article':{
                numericValueWithOutZero:true
                
            },
            
            
        },
        messages: {
            writer_type: {
                required: "Please choose at least one radio button",                
            }
        },
        submitHandler: function (form) {
            var min_article = $('input[name="min_article"]').val();
            var max_article = $('input[name="max_article"]').val()
            var isPaymentVerifyError = false;
            if (min_article && max_article) {              
                if (parseInt(min_article) >= parseInt(max_article)) {                   
                    var label = '<label id="max_article-error" class="error CWError" >To article  field must be greater than  From article field</label>';
                    $("#no_of_article_per_month").parent().parent().append(label);
                    isPaymentVerifyError = true;
                    return false;
                }
            }
            
            if(isPaymentVerifyError===false){
                var isPriceTypeExist = $("#ispricetypeexist").val();
                if (isPriceTypeExist) {
                    swal({
                        title: "",
                        text: "If you change the pay type of the user then contract will be renewed and freelancer has to self attest the contract and upload the contract also. Hard copy of the contract will be couriered at Times Internet after that revised payment be released only",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, change it!",
                        closeOnConfirm: false
                    }, function () {
                        form.submit();
                    });
                }else{
                    form.submit();
                }
                
            }else{            
                return false;
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });
    
    $('#profile_form_validation').validate({
        rules: {
            'checkbox': {
                required: true
            },
            'mobile': {
                required: true,
                validMobileNumber:true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            'name': {
                required: true,
                alphabeticValue:true,
                normalizer: function (value) {
                    return $.trim(value);
                }
            },
            
            
            
        },
        submitHandler: function () {
            $('.sbtbtn').prop('disabled', true);
            var optionsCW = $('#content_writer_lang_id > option:selected');
            var optionsCTF = $('#content_trans_from_id > option:selected');
            var optionsCTT = $('#content_trans_to_id > option:selected');
            //var subcategory = $('#subcategory > option:selected');
            var category = $('#category > option:selected');
            
            if(parseInt(optionsCW.length) === 0 && parseInt(optionsCTF.length) === 0 && parseInt(optionsCTT.length) === 0){
                var label = '<label id="content_writer_lang_id-error" class="error CWError" >Please select either content writing or content translation field!</label>';
                $("#content_writer_lang_id").parent().parent().append(label); 
                $('.sbtbtn').prop('disabled', false);
                return false;
            }else if(parseInt(optionsCW.length) > 0 && parseInt(optionsCTF.length) > 0 && parseInt(optionsCTT.length) === 0){
                var label = '<label id="content_trans_to_id-error" class="error CWError" >Please select content translation to field!</label>';
                $("#content_trans_to_id").parent().parent().append(label);  
                 $('.sbtbtn').prop('disabled', false);
                return false;
            }else if(parseInt(optionsCW.length) > 0 && parseInt(optionsCTF.length) === 0 && parseInt(optionsCTT.length) > 0){
                var label = '<label id="content_trans_from_id-error" class="error" >Please select content translation from field!</label>';
                $("#content_trans_from_id").parent().parent().append(label); 
                 $('.sbtbtn').prop('disabled', false);
                return false;
            } else if(parseInt(optionsCW.length) === 0 && parseInt(optionsCTF.length) === 0 && parseInt(optionsCTT.length) > 0){
                var label = '<label id="content_trans_from_id-error" class="error" >Please select content translation from field!</label>';
                $("#content_trans_from_id").parent().parent().append(label); 
                 $('.sbtbtn').prop('disabled', false);
                return false;
            }else if(parseInt(optionsCW.length) === 0 && parseInt(optionsCTF.length) > 0 && parseInt(optionsCTT.length) === 0){
                var label = '<label id="content_trans_to_id-error" class="error" >Please select content translation to field!</label>';
                $("#content_trans_to_id").parent().parent().append(label); 
                $('.sbtbtn').prop('disabled', false);                
                return false;
                
            }
            if (parseInt(optionsCTF.length) === 1 && parseInt(optionsCTT.length) === 1) {
                if (optionsCTF[0].value === optionsCTT[0].value) {
                    var label = '<label id="content_trans_to_id-error" class="error" >Content translation  from and to field should not be same!</label>';
                    $("#content_trans_to_id").parent().parent().append(label);
                    $('.sbtbtn').prop('disabled', false);
                    return false;
                }

            }
            /*if(parseInt(category.length) === 0 ){
                var label = '<label id="category-error" class="error" >Please select category field!</label>';
                $("#category").parent().parent().append(label); 
                $('.sbtbtn').prop('disabled', false);                
                //return false;
                
            }else if(parseInt(category.length) > 6){
                var label = '<label id="category-error" class="error" >Category field should be less than or equal to 2!</label>';
                $("#category").parent().parent().append(label); 
                $('.sbtbtn').prop('disabled', false);                
                return false;
            }*/
            
            /*if(parseInt(subcategory.length) === 0 ){
                var label = '<label id="subcategory-error" class="error" >Please select subcategory field!</label>';
                $("#subcategory").parent().parent().append(label); 
                $('.sbtbtn').prop('disabled', false);                
                //return false;
                
            }else if(parseInt(subcategory.length) > 6){
                var label = '<label id="subcategory-error" class="error" >Subcategory field should be less than or equal to 6!</label>';
                $("#subcategory").parent().parent().append(label); 
                $('.sbtbtn').prop('disabled', false);                
                return false;
            }*/
         //return false;
            $.ajax({
                type: "POST",
                url: $("#profile_form_validation").attr("action"),
                data: $("#profile_form_validation").serialize(),
                dataType:'JSON',
                headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
                 success: function (responseData) {
                     $('.sbtbtn').prop('disabled', false);
                     if(responseData.response.status == true){
                         window.location.href= responseData.response.reditectUrl;
                     }else if (responseData.response.status == false) {
                        var li = '';
                        $.each(responseData.response.message, function (index, val) {
                            console.log(val)
                            li += '<li>' + val + '</li>';
                        });
                        var successMsg = '<div class="alert alert-danger alert-block "><button type="button" class="close" data-dismiss="alert">×</button><strong><ul>' + li + '</ul></strong></div>';
                        $(".successMsgDiv").html(successMsg);
                        $(".successMsgDiv").show();
                        $("html, body").animate({scrollTop: 0}, "slow");

                    }
                 }
             });
             return false; 
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    //Advanced Form Validation
    $('#form_advanced_validation').validate({
        rules: {
            'date': {
                customdate: true
            },
            'creditcard': {
                creditcard: true
            }
        },
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    //Custom Validations ===============================================================================
    //Date
    $.validator.addMethod('customdate', function (value, element) {
        return value.match(/^\d\d\d\d?-\d\d?-\d\d$/);
    },
        'Please enter a date in the format YYYY-MM-DD.'
    );

    //Credit card
    $.validator.addMethod('creditcard', function (value, element) {
        return value.match(/^\d\d\d\d?-\d\d\d\d?-\d\d\d\d?-\d\d\d\d$/);
    },
        'Please enter a credit card in the format XXXX-XXXX-XXXX-XXXX.'
    );
    // Pan Card 
    $.validator.addMethod('validPanCard', function (value, element) {
        //regex:/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/
        return value.match(/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/);
    },
        'Please enter valid pan card number'
    );
    // Numeric Validation 
    $.validator.addMethod('numericValue', function (value, element) {
        //regex:/^[0-9]*$/
        return value.match(/^[0-9]*$/);
    },
        'Please enter numeric value'
    );
    
    // Pin code Validation 
    $.validator.addMethod('PinCode', function (value, element) {
        //regex:/^[0-9]*$/
        return value.match(/^[0-9]*$/);
    },
        'Please enter valid pin code'
    );
    // Numeric Validation 
    $.validator.addMethod('numericValueWithOutZero', function (value, element) {
        //regex:/^[0-9]*$/
        return value.match(/^[1-9]\d*$/);
    },
        'Please enter numeric value greater than zero (0)'
    );
    // alpha Numeric Validation 
    $.validator.addMethod('alphaNumericValue', function (value, element) {
        //regex:/^[a-zA-Z0-9\s]*$/
        return value.match(/^[a-zA-Z0-9\s]*$/);
    },
        'Please enter alphanumeric value'
    );
    
    // address Validation 
    $.validator.addMethod('addressValidation', function (value, element) {
        //regex:/^[a-zA-Z0-9\s]*$/
        return value.match(/^[a-zA-Z0-9\s,-.:/&]*$/);
    },
        'Please enter valid address'
    );
    
    // Alphabetic Validation 
    $.validator.addMethod('alphabeticValue', function (value, element) {
        //regex:/^[a-zA-Z0-9\s]*$/
        return value.match(/^[a-zA-Z\s]*$/);
    },
        'Please enter alphabets only'
    );
    $.validator.addMethod('priceWithDecimal', function (value, element) {
        //regex:/^[a-zA-Z0-9\s]*$/
        return value.match(/^\d{0,10}(\.\d{0,4})?$/);
    },
        'Please enter numeric or decimal value'
    );
    //==================================================================================================
     $.validator.addMethod( "maxFileSize", function( value, element,params ) {
	if ( this.optional( element ) ) {
		return true;
	}

	if ( $( element ).attr( "type" ) === "file" ) {
		if ( element.files && element.files.length ) {
			for ( var i = 0; i < element.files.length; i++ ) {
				if ( element.files[ i ].size > params ) { 
					return false;
				}
			}
		}
	}

	return true;
    }, $.validator.format( "File size must not exceed 5 MB." ) );
    //====================================================================================================================
    // Accept a value from a file input based on a required mimetype
    $.validator.addMethod( "fileType", function( value, element, param ) {
        //alert(param);
	// Split mime on commas in case we have multiple types we can accept
	var typeParam = typeof param === "string" ? param.replace( /\s/g, "" ) : "image/*",
		optionalValue = this.optional( element ),
		i, file, regex;

	// Element is optional
	if ( optionalValue ) {
		return optionalValue;
	}

	if ( $( element ).attr( "type" ) === "file" ) {

		// Escape string to be used in the regex
		// see: https://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
		// Escape also "/*" as "/.*" as a wildcard
		typeParam = typeParam
				.replace( /[\-\[\]\/\{\}\(\)\+\?\.\\\^\$\|]/g, "\\$&" )
				.replace( /,/g, "|" )
				.replace( /\/\*/g, "/.*" );
                        //alert(typeParam);
		// Check if the element has a FileList before checking each file
		if ( element.files && element.files.length ) {
			regex = new RegExp( ".?(" + typeParam + ")$", "i" );
			for ( i = 0; i < element.files.length; i++ ) {
				file = element.files[ i ];

				// Grab the mimetype from the loaded file, verify it matches
				if ( !file.type.match( regex ) ) {
					return false;
				}
			}
		}
	}

	// Either return true because we've validated each file, or because the
	// browser does not support element.files and the FileList feature
	return true;
    }, $.validator.format( "Please enter a value with a valid file.(jpeg,jpg,png,pdf)" ) );
    //====================================================================================================================

    $.validator.addMethod('validateTinyMceData', function (value, element) {
        console.log(value,'VAL');
        console.log(element,'element');
        var theEditor = tinymce.activeEditor;
            var content = theEditor.getContent();
            console.log(content,'content');
            if (content === "" || content === null) {
                
                return false;
            }
    },
        'Content field is required'
    );
    $.validator.addMethod('checkVideoUploadBtn', function (value, element) {
        $('#btnSubmit').hide();
    },'');
    $.validator.addMethod('validMobileNumber', function (value, element) {
       if(value.length ==10){
           return value.match(/^[0-9]*$/);
       }else{
           return false;
       }
    },'Invalid mobile number.');


});
