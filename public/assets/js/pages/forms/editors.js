$(function () {
    //CKEditor
   //CKEDITOR.replace('ckeditor');
    //CKEDITOR.config.height = 300;
    var taskWordCount = contentWordCount;
    var writerTaskWordCount = word_count;
    var tinyMceTheme = "modern";
    var  pluginArr = [
            'advlist autolink lists link image charmap print preview hr ',
            'searchreplace  visualblocks visualchars code fullscreen',
            'media save table contextmenu directionality',
            'emoticons paste textcolor colorpicker textpattern imagetools image code'
        ];
if(isMobile==true){
    tinyMceTheme = "mobile";
    pluginArr = [
           //'wordcount'
        ];
}
    //TinyMCE
    tinymce.init({
        selector: "textarea#tinymce",
        branding: false,
        content_style: ".mce-content-body {font-size:15px;font-family:Arial,sans-serif;} .mce-content-body .extra-content-color-red{color:#ff0000}",
        theme: tinyMceTheme,
        force_br_newlines : true,
        force_p_newlines : false,
        forced_root_block :'',
        image_caption: true,
        image_description:true,
        image_title: false,
        height: 300,
        media_poster: false,
        media_alt_source: false,
        entity_encoding: 'raw',
       // language: 'ta_IN',
        video_template_callback: function(data) {

            return '<iframe src="' + data.source1 + '" style="height: 100%; width: 100%; max-height: 100%; max-width: 100%; visibility: visible;" ></iframe>';
        },       
        plugins: pluginArr,
        
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image ',
        toolbar2: 'print preview media | forecolor backcolor',
        
        image_advtab: true,
        convert_urls: false,
        //images_upload_credentials :true,
        images_upload_url: tLoginObj.home_url+'/uploadTinyMceImage',
        // override default upload handler to simulate successful upload
        images_upload_handler: function (blobInfo, success, failure) {
             if (!blobInfo.blob().type.match(/.?(jpeg|jpg|png)$/)) {
                console.log('type');
                failure('Error : Invalid file type. File type should be (jpeg,jpg,png) and max size 5MB'  );
                return false;
            }else if (blobInfo.blob().size >5120000) {
                console.log('size');
                failure('Error : nvalid file size. File size should be 5MB of type (jpeg,jpg,png)'  );
                return false;
            }
            var xhr, formData;

            xhr = new XMLHttpRequest();
           // xhr.withCredentials = false;
            xhr.open('POST', tLoginObj.home_url+'/uploadTinyMceImage');
            xhr.setRequestHeader('X-CSRF-TOKEN',$('meta[name="csrf-token-tme"]').attr('content'));
            xhr.onload = function() {
                var json;        
                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }        
                if (xhr.responseText) {
                    json = JSON.parse(xhr.responseText);
                     if(json.status==false){
                        failure('Error: ' + json.msg);
                        return;
                    }else if(json.status==true){
                        success(json.location);
                    }
                    
                }else{
                    failure('HTTP Error: ' + xhr.status );
                    return;
                }
            };

            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        },
        setup: function (ed) {
            
             $('.totalWord').remove();
             //$('.exceedHtml').remove();
                ed.on('keyup', function (e) {
                    $('.totalWord').remove();    
                    var theEditor = tinymce.activeEditor;
                    //var writtenWords = theEditor.plugins.wordcount.getCount();
                    //console.log(writtenWords,'writtenWords');
                   // var writtenWords = $('.mce-wordcount').html();
                    $('.mce-wordcount').css('padding-right' ,'75px');
                    //writtenWords = writtenWords.replace("words", "");
                    var maxWord = taskWordCount//ed.settings.max_word;
                    var limited = "";
                    var exceedHtml = "";
                    var finalContent = "";
                    var content = ed.getContent();
                    console.log(content,'content core');
                    var contentwordCount =0
                    if (content) {
                        var contentStr = content;
                        contentStr = contentStr.toString();
                        contentStr = contentStr.replace(/<br\s*[\/]?>/gi, ' ');
                        
                        var temporalDivElement = document.createElement("div");
                        temporalDivElement.innerHTML = contentStr;
                        contentStr =temporalDivElement.textContent || temporalDivElement.innerText || ""
                        
                        contentStr = contentStr.replace(/\n/, " ");
                        contentStr = contentStr.replace(/\s{2,}/g, ' ');
                        contentStr = contentStr.replace(/&nbsp;/g, "");
                        contentStr = contentStr.replace(/\r /," ");
                        
                        //contentStr = contentStr.replace(/<[^>]*>/g, '');
                       /* contentStr = contentStr.replace(/<\/?span[^>]*>/g, "");
                        contentStr = contentStr.replace(/<\/?p[^>]*>/g, "");
                        contentStr = contentStr.replace(/<\/?div[^>]*>/g, "");
                        contentStr = contentStr.replace(/<\/?a[^>]*>/g, "");
                        contentStr = contentStr.replace(/<\/?img[^>]*>/g, "");
                        contentStr = contentStr.replace(/&nbsp;/g, "");
                        contentStr = contentStr.replace(/<br \/>/, " ");
                        contentStr = contentStr.replace(/<br\s*[\/]?>/gi, ' ');
                        contentStr = contentStr.replace(/\s{2,}/g, ' ');
                        contentStr = contentStr.replace(/\n /, " ");
                        contentStr = contentStr.replace(/\r /," ");
                        */
                        
                        
                        contentStr = contentStr.trim();
                        if (contentStr.match(/\s+/g)) {
                            contentwordCount = contentStr.match(/\s+/g).length;
                            contentwordCount = contentwordCount + 1;
                        } else {
                            contentwordCount = 1;
                        }
                        //console.log(contentwordCount,'content');
                        
                    }
                    
                    console.log(contentStr,'content');
                    console.log(contentwordCount,'contentwordCount');
                    if ($('.mce-wordcount').length) {
                        $('.mce-wordcount').text(contentwordCount + " words");
                        $('.mce-wordcount').before('<span class="totalWord" style="float: right;padding-top: 10px;padding-right: 5px;">/' + maxWord + ' words</span>');

                    }else{
                        $('.mce-path').after('<span id="mceu_45" class="mce-wordcount mce-widget mce-label mce-flow-layout-item">'+contentwordCount+' words</span>');
                        $('.mce-wordcount').before('<span class="totalWord" style="float: right;padding-top: 10px;padding-right: 5px;">/' + maxWord + ' words</span>');
                    }
                    
                    if (contentwordCount > maxWord && maxWord>0) {                        
                        //$('.mce-wordcount').css("color", "red");
                        limited = $.trim(content).split(" ", maxWord);
                        limited = limited.join(" ");
                        limited = limited+" ";
                        exceedHtml  =  $.trim(content).replace(limited, '');
                        exceedHtml = exceedHtml.trim();
                        //console.log(exceedHtml,'exceedHtml');
                        //console.log(exceedHtml.search("data-ex"),'yes');
                        if (exceedHtml.search("data-ex")===-1) {
                            exceedHtml = exceedHtml.replace(/<\/?span[^>]*>/g, "");
                            //exceedHtml = "<span data-ex style='color:#ff0000'>" + exceedHtml + "</span>";
                            exceedHtml = "<span data-ex class='extra-content-color-red'>" + exceedHtml + "</span>";
                            finalContent = limited + " " + exceedHtml;
                            ed.setContent(finalContent);
                        }
                        console.log(finalContent,'finalContent');
                        
                    } else {
                        //$('.mce-wordcount').css("color", "green");
                    }
                });
                ed.on('keyup', function (e) {
                    /*console.log('keyup');    
                    var content = ed.getContent();
                    if(content){
                        var contentStr = content;
                    contentStr = contentStr.toString();
                    console.log(contentStr);
                    contentStr = contentStr.replace(/<[^>]*>/g, '');
                    contentStr = contentStr.trim();
                    //console.log(contentStr);
                    var contentwordCount = 0;
                    console.log(contentStr.match(/\s+/g).length,'match');
                    if(contentStr.match(/\s+/g).length){
                         contentwordCount = contentStr.match(/\s+/g).length;
                    }else{
                          contentwordCount = contentStr;
                    }
                   
                    //console.log(contentwordCount,'content');
                    $('.mce-wordcount').text(contentwordCount+1+" words");
                    }*/
                    
                    
                });
                ed.on('keydown', function (e) {
                    //console.log('keydown');    
                    /*var content = ed.getContent();
                    var contentStr = content;
                    contentStr = contentStr.toString();
                    //console.log(contentStr);
                    contentStr = contentStr.replace(/<[^>]*>/g, '');
                    contentStr = contentStr.trim();
                    //console.log(contentStr);
                    var contentwordCount = contentStr.match(/\s+/g).length;
                    //console.log(contentwordCount,'content');
                    $('.mce-wordcount').text(contentwordCount+1+" words");
                    */
                    
                });
                ed.on('load', function (e) {
                     $('.mce-path').after('<span id="mceu_45" class="mce-wordcount mce-widget mce-label mce-flow-layout-item">0 words</span>');
                    
                    setTimeout(function(){ 
                        var content = ed.getContent();
                        if(content){
                            console.log(content,'content load');
                            writerTaskWordCount = writerTaskWordCount+" words"
                            $('.mce-wordcount').text('');
                            $('.mce-wordcount').text(writerTaskWordCount) 
                        }
                      
                    }, 2000);                    
                    
                });
            },
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = '../../assets/plugins/tinymce';
});