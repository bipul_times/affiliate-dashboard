var langchannel=tLoginObj.ssochannel;
var newlogin='true';
var urlpath = document.location.pathname;

function Get_Ckie(name){
    var start=document.cookie.indexOf(name+"=")
    var len=start+name.length+1
    if((!start)&&(name !=document.cookie.substring(0,name.length))){
            return null
    }
    if(start==-1)return null;
    var end=document.cookie.indexOf(";",len)
    if(end==-1)end=document.cookie.length
    return unescape(document.cookie.substring(len,end))
}	

                                     
(function(event){
    var default_config = {
        autoinit: true,
        multiuser: false, //allows multiple users to login at a time
        login: "",
        logout: "",
        check_user_status: "",
        mapping: null,
        renderer: true, //todo to be implemented
        default_user_icon:tLoginObj.pluginsUrl+"/toi-t-login/img/color_profile.png"
    };
    var userInfoconfig={
        fullname :'',
        fstname :'',
        email :'',
        sso :'',
        city :'',
        D_N_U :'',
        loggedstatus : 0,
        _id : '',
        thumb : '',
        SIU :'',
        facebookimg :'',
        defaultimg :tLoginObj.pluginsUrl+'/toi-t-login/img/color_profile.png'
    };
    var config = $.extend(true, {}, default_config);
        
    var errorConfig = {
        fpNoEmailOrMobile: 'Email/Mobile No. cannot be left blank.',
        fpInvalidEmail: 'Enter a valid Email/Mobile No.',
        fpInvalidEmailOnly: 'Enter a valid email.',
        fpInvalidMobileOnly: 'Enter a valid mobile no.',
        accountUnregistered: 'This account is not registered with us.',
        emptyPassword: 'Password cannot be left blank.',
        emptyName: 'Enter your full name.',
        wrongName: 'Enter your name without special character or numbers.',
        wrongPassword: 'Invalid Credentials',
        wrongMobile: 'Invalid Mobile number.',
        wrongEmail: 'Invalid Email Id.',
        expiredOTP: 'The OTP you entered has expired.',
        limitExceeded: 'Maximum number of unsuccessful attempts exceeded. Please try again later.',
        wrongOtp: 'Enter valid OTP sent to your mobile no.',
        wrongOtpEmail: 'Enter valid OTP sent to your email.',
        matchLastThree: 'Password cannot match your last three passwords.',
        passwordMismatch: 'Your passwords do not match.',
        captchaUnselected: 'Select the checkbox to proceed.',
        tncNotSelected: 'Accept Terms & Conditions to proceed.',
        userAlreadyRegistered: 'This user is already registered with us.',
        serverError: 'An error occurred while processing your request. Please try again later.',
        connectionError: 'There was an error processing your request. Please check your internet connection and try again.',
        email_failure:"Username or password is incorrect. Please try again.",
        facebook_failure:"Username or password is incorrect. Please try again.",
        facebook_failure_user_denied:"Kindly authenticate facebook request. Please try again.",
        facebook_failure_no_email:"Could not login using this facebook account. Please try again with another account.",
        twitter_failure:"Kindly authenticate twitter request.",
        twitter_link_username_failure:"Username or password is incorrect. Please try again.",
        twitter_link_email_failure:"Email or password is incorrect. Please try again.",
        twitter_failure_unknown:"Kindly authenticate twitter request.",
        twitter_failure_user_denied:"Username or password is incorrect. Please try again.",
        twitter_failure_server_error:"Server did not respond. Please try again.",
        twitter_failure_already_exist:"It seems you have already registered with the Indiatimes network using this Email ID. Try linking your Twitter account with your existing Indiatimes account or use an alternate Email ID.",
        twitter_failure_invalid_email:"It seems you have entered and invalid Email ID. Try linking your Twitter account with your existing Indiatimes account or use an alternate Email ID.",
        unknown_error:"Unknown error has occurred. Please try again."
    };
    
    var redirect_uri = tLoginObj.home_url+'/t-login-sso';        
    //tLoginObj.ssoUrl+"/sso/identity/login/socialLogin?channel=##$channel##&oauthsiteid=##$siteid##&ru="+redirect_uri,
    //
    var constants = {
        channel:langchannel,
        oauthUrlFacebook: 'https://www.facebook.com/v3.1/dialog/oauth?display=popup&scope=email%2Cuser_birthday%2Cuser_hometown&client_id='+tLoginObj.fbclient+'&state={"oauthsiteid":"##$siteid##"}&redirect_uri='+redirect_uri,
        oauthUrlGooglePlus: 'https://accounts.google.com/o/oauth2/auth?response_type=code&scope=email%20https://www.googleapis.com/auth/plus.login&access_type=online&client_id='+tLoginObj.gPlusclient+'&state={"oauthsiteid":"##$siteid##"}&redirect_uri='+redirect_uri,
        logoutUrl : tLoginObj.ssoUrl+"/sso/identity/profile/logout/external?channel=##$channel##"
    };

    var cachedElements = {
        loginPopup: $('#login-popup'),
        closeBtn: $('#login-popup .close-btn')
    }

    var ssoLoginType = '', 
        jssoCrosswalkObj, 
        recaptchaCode = '', 
        recaptchaWidgetId = '', 
        pageName='', 
        screenName='Login_Screen',
        registerFormSubmitted = false;

    var userList = {};

    var single_user_id = "SINGLE_USER";

    var call_sso = function () {
        if (localStorage.getItem('_ssodata') != null) {
            __sso(JSON.parse(localStorage.getItem('_ssodata')));
            localStorage.removeItem('_ssodata');
        }
    }
    // to handle ios chrome issue - where parent is undefined
    var sso = function (url, callback) {

        console.log('url===', url);
        loginWindow = window.open(url, '', 'width=500, height=500,scrollbars=0');
        loginWindow.moveTo(315, 250);
        //loginWindow.close();
    };
    var mod_login = {};
    var loginCallback = null;
    var loginData = null;
    var setLoginError = function (loginErrorMsg) {
        setLoginData({
            error: {
                code: loginErrorMsg,
                message: config.messages[loginErrorMsg]
            }
        });
    };
    var setLoginData = function (data) {
        loginData = data;
    };
    var reset = function(){
        return;
        if (loginWindow) {
            loginWindow.close();
            loginWindow = null;
        }
        loginData = null;
    };

    //edit function from toi
    var setLocation = function (href) {
        //if (is.iframe(loginWindow)) {
          //  loginWindow.src = href;
        //} else {
            loginWindow.document.location.href = href;
        //}
    };
    var loginResponse = function (site, error) {
        if(site === 'facebook') {
            $("#sso-fblogin-error").html(errorConfig[error]).show();
        } else {
            $("#sso-gplus-error").html(errorConfig[error]).show();
        }         
    };

    var loginResponseTwitter = function (url, error, data) {
        setLocation(url);
        if (error) {
            setLoginError(error);
        } else if (data) {
            setLoginData(data);
        }
    };

    var loginWindow = null;
    var setCriOS;

    try {
        document.domain = document.domain;
    } catch (e) {
        console.log("Domain cannot be set:" + document.domain);
    }
        
    // to handle ios chrome issue - where parent is undefined
    if (navigator.userAgent.match('CriOS')) {
        localStorage.removeItem('_ssodata');
        setCriOS = setCriOS || setInterval(function () {
                call_sso();
            }, 1000);
    }

    window.getLoginCallback = function () {
        return loginCallback;
    };
    window.getLoginData = function () {
        return loginData;
    };
    window.getDomain = function () {
        document.domain;
    };

    window.__sso = function (data, url) {
        console.log('ddd');
        console.log(data);
        console.log('xxx');

        var $loginPopup = $('#login-popup');
        if (data && loginWindow) {
            //logger.log(url);
            var currLoginData = loginData ? loginData.data : null;
            var closeWindow = data.closeWindow,
                isLogout = data.status === "logout",
                signInSuccess = data.status === "signinsuccess" || data.status === "ssosigninsuccess" || data.status === "SUCCESS",
                fbOrGplusMappingUpdated = (data.site === "facebook" || data.site === "googleplus" ) && data.status === "MappingUpdated";
            data.site = data.site || "";
            if (closeWindow || isLogout || signInSuccess || fbOrGplusMappingUpdated) {
                //logger.log("Close pressed, closing window / popup");
                loginWindow.close();
                $("#sso-fblogin-error, #sso-gplus-error").html('').hide();
                if($loginPopup.is(':visible')) {
                    mod_login.closeBtnHandler();
                }

                if(data.site && (signInSuccess || fbOrGplusMappingUpdated)) {
                    mod_login.fireGAEvent(data.site === 'googleplus'? 'Login_Success_Google': 'Login_Success_FB');
                }
            } else if (data.status == "ssosigninfailure" && data.ssoerror == "E119") { //E119-username with password incorrect
                loginResponse(data.site, "email_failure");
            } else if (data.status == "ssosigninfailure" && data.ssoerror == "E104") { //E104-email with password incorrect
                loginResponse(data.site, "email_failure");
            } else if (data.err == "E104" && data.facebooktoken) { //E104-facebook not sending email
                loginResponse(data.site, "facebook_failure_no_email");
            } else if (data.status == "signinfailure" && data.error == "F101") {
                loginResponse(data.site, data.site + "_failure");
            } else if (data.status == "signinfailure" && data.error == "user_denied" && data.site == "facebook") { //facebook user denied access to the account
                loginResponse(data.site, "facebook_failure_user_denied");
            }  else {
                //logger.warn("Login case not handled");
                loginResponse(data.site, "unknown_error");
            }
        }
        console.log("Checking user status in __sso");
        mod_login.isLoggedIn(loginCallback);
    };
        
    mod_login.closeWindow = window.closeLoginWindow = function () {
        if (loginWindow) {
            loginWindow.close();
        }
    };

    mod_login.fireGAEvent = function (label) {
        label += ('-' + window.location.pathname);
        //ga('send', 'event', 'WEB_Login', mod_login.getScreenName(), label);
        if(typeof ga != "undefined")
            ga('send', 'event', 'WEB_Login', 'click', label);
    };
        
    /*Returns false if browser is Opera*/
    mod_login.showCaptcha = function () {
        return (navigator.userAgent.indexOf("Opera") === -1);
    };

    mod_login.registerFormSubmitted = function (isSubmitted) {
        registerFormSubmitted = !!isSubmitted;
    };

    mod_login.setLoginWindowDimension = window.setLoginWindowDimension = function (width, height) {
        if (loginWindow) {
            loginWindow.resizeTo(width, height);
            if (loginWindow.reposition) { //TODO not working for window.open - popup
                loginWindow.reposition();
            }
            loginWindow.focus();
        }
    };
    mod_login.login = function (callback, action, identifier) {
        //TimesApps.checkGdprAndCall(function() {
            mod_login.showLoginScreen(callback);
            $('#login-popup').addClass('active');
        //}, mod_login.logout);

    };
    mod_login.loginWithTwitter = function (callback) {
        loginCallback = function (user) {
            if(typeof ga != "undefined")
            ga('send', 'event', 'Facebook', 'click', 'LoginwtihTWT');
            if (callback) {
                callback(user);
            }
            mod_login.closeBtnHandler();
        };   
        reset();

        //TimesApps.checkGdprAndCall(function(){
            var login_url = constants.oauthUrl.replace('##$channel##', constants.channel).replace('##$siteid##', 'twitter');
            sso(login_url, callback);
        //}, mod_login.logout);
    };
        
    mod_login.loginWithFacebook = function (callback) {
        loginCallback = function (user) {
           if(typeof ga != "undefined")
            ga('send', 'event', 'Facebook', 'click', 'LoginwtihFB');
            if (callback) {
                callback(user);
            }
            mod_login.closeBtnHandler();
        };    
        reset();
        mod_login.initiateFbLogin(callback);
    };
        
    // Initiate FB Login
    mod_login.initiateFbLogin = function (callback) {
        console.log('FBFBFBFBFB');
        //TimesApps.checkGdprAndCall(function(){
            mod_login.setScreenName('Login_Screen');
            mod_login.fireGAEvent('Click_FB');
            var login_url = constants.oauthUrlFacebook;
            login_url = login_url.replace('##$siteid##', 'facebook');
            console.log('login_url');
            console.log(login_url);
            console.log('login_url');
            sso(login_url, callback);
        //}, mod_login.logout);
    };
        
    mod_login.loginWithGoogle = function (callback) {
        loginCallback = function (user) {
            if(typeof ga != "undefined")
            ga('send', 'event', 'googleplus', 'click', 'Loginwtihgplus');
            if (callback) {
                callback(user);
            }
            mod_login.closeBtnHandler();
        };   
        reset();
        mod_login.initiateGplusLogin(callback);
    };
        
    // Initiates Google login 
    mod_login.initiateGplusLogin = function ( callback ) {
        //TimesApps.checkGdprAndCall(function(){
        mod_login.setScreenName('Login_Screen');
        mod_login.fireGAEvent('Click_Google');
        var login_url = constants.oauthUrlGooglePlus.replace('##$siteid##', 'googleplus');
        console.log('Gplus='+login_url);
        sso(login_url, callback);
    //}, mod_login.logout);
    };
        
    /*Creates Login Screen UI and inserts in page*/
    mod_login.showLoginScreen = function (callback) {
        var loginScreen = '';
        loginScreen +=  '<div id="lang_login">'
                    +       '<div class="signin-section">'
                    +           '<figure class="user-icon"><img src="https://timesofindia.indiatimes.com/photo/63379366.cms" src="user-icon" /></figure>'
                    +           '<div id="socialConnectImgDiv">'
                    +               '<button type="button" id="sso-fb-login" class="fb">Sign in with Facebook</button>'
                    +               '<span id="sso-fblogin-error" class="errorMsg"></span>'
                    +               '<button type="button" id="sso-gplus-login" class="gplus">Sign in with Google</button>'
                    +               '<span id="sso-gplus-error" class="errorMsg"></span>'
                    +           '</div>'
                    +           '<h4 class="heading small">'
                    +               '<span>or go the traditional way</span>'
                    +           '</h4>'
                    +           '<form class="form" autocomplete="off">'
                    +               '<ul>'
                    +                   '<li class="input-field email">'
                    +                       '<p>'
                    +                           '<input autocomplete="off" type="text" name="emailId" placeholder="Sign In/Sign Up with Email or Mobile No." maxlength="100" />'
                    +                       '</p>'
                    +                       '<div class="errorMsg"></div>'
                    +                       '<a href="javascript:void(0)" id="changeEmailIdDiv" class="secondary-link" style="display: none">Change Email Or Mobile No.</a>'
                    +                   '</li>'
                    +                   '<li class="input-field password" id="sso-pwdDiv">'
                    +                       '<p>'
                    +                           '<input autocomplete="off" type="password" name="password" placeholder="Password" maxlength="14" />'
                    +                           '<span class="view-password"></span>'
                    +                       '</p>'
                    +                       '<div class="errorMsg"></div>'
                    +                       '<a id="sso-generate-otp" href="javascript:void(0)" class="secondary-link">Generate OTP to Login</a>'
                    +                   '</li>'
                    +                   '<li id="sso-login-otp-msg" class="text-field">'
                    +                       '<p></p>'
                    +                   '</li>'
                    +                   '<li class="input-field password" id="sso-otpLoginDiv">'
                    +                       '<p>'
                    +                           '<input type="password" name="otplogin" maxlength="6" placeholder="Enter the verification code"/>'
                    +                       '</p>'
                    +                       '<div class="errorMsg"></div>'
                    +                       '<div class="successMsg"></div>'
                    +                       '<span class="regenerate-otp">Didn\'t receive OTP?</span>'
                    +                       '<a id="sso-regenerate-otp" href="javascript:void(0)" class="secondary-link">Re-Generate OTP</a>'
                    +                   '</li>'
                    +                   '<li id="sso-signInButtonDiv" class="submit">'
                    +                       '<input type="submit" class="submit-btn disabled" value="Continue" disabled="disabled" />'
                    +                   '</li>'
                    +               '</ul>'
                    +               '<a href="javascript:void(0)" id="sso-forgot-pass" class="forget-password">Forgot Password?</a>'
                    +           '</form>'
                    +       '</div>'
                    +       '<div class="powered-by">'
                    +           '<img src="https://static.toiimg.com/photo/65257504.cms" />'
                    +       '</div>'
                    +       '<div class="teams-logo">'
                    +          '<span>One Network. One Account</span>'
                    +           '<a href="http://timesofindia.indiatimes.com/" target="_blank" class="toi"/><a href="http://economictimes.indiatimes.com/" target="_blank" class="et"/><a href="http://navbharattimes.indiatimes.com/" class="nbt" target="_blank"/><a href="http://maharashtratimes.indiatimes.com/" class="sm" target="_blank"/><a href="http://www.speakingtree.in/" class="st" target="_blank"/><a href="http://gaana.com/" class="gaana" target="_blank"/><a href="http://itimes.com/" class="itimes" target="_blank"/><a href="http://www.timespoints.com/#/login" class="tp" target="_blank"/>'
                    +       '</div>'
                    +   '</div>';
        $('#signin_box').html(loginScreen);
    };
        
    /*Creates Register Screen UI and inserts in page*/
    mod_login.showRegisterScreen = function (callback) {
        var inputVal = $('#lang_login input[name="emailId"]').val();
        var loginType = mod_login.getLoginType();
        var registerScreen = '';
        registerScreen  +=      '<div id="lang_register">'
                        +           '<div class="signup-section">'
                        +               '<h4 class="heading">'
                        +                   '<span>Complete Your Profile</span>'
                        +               '</h4>'
                        +               '<form class="form" action="#" autocomplete="off">'
                        +                   '<input type="hidden" id="register-inputVal" value="' + inputVal + '"/>'
                        +                   '<ul>'
                        +                       '<li class="input-field ' + (loginType !== 'email'? 'mobile-no': 'email') + '">'
                        +                           '<p>'
                        +                               (loginType !== 'email'? '<span class="country-code">+91 - </span>': '')
                        +                               '<input autocomplete="off" type="text" name="' + (loginType === 'email'? 'emailId': 'mobile') + '" maxlength="100" disabled="disabled" value="' + inputVal  + '" />'
                        +                           '</p>'
                        +                           '<a href="javascript:void(0)" id="changeRegisterEmailId" class="secondary-link">Change Email Or Mobile No.</a>'
                        +                       '</li>'
                        +                       '<li class="input-field user-name">'
                        +                           '<p>'
                        +                               '<input autocomplete="off" type="text" name="fullname" placeholder="Full Name" maxlength="30" />'
                        +                           '</p>'
                        +                           '<div class="errorMsg"></div>'
                        +                       '</li>'
                        +                       '<li class="input-field password">'
                        +                           '<p>'
                        +                               '<input autocomplete="off" type="password" name="registerPwd" placeholder="Password" maxlength="14" />'
                        +                               '<span class="view-password"></span>'
                        +                           '</p>'
                        +                           '<div class="password-conditions">'
                        +                               '<p>Password must have:</p>'
                        +                               '<ul>'
                        +                                   '<li id="charCnt" class="error">6-14 characters</li>'
                        +                                   '<li id="lwCnt" class="error">1 Lower case character (a-z)</li>'
                        +                                   '<li id="numCnt" class="error">1 Numeric character (0-9)</li>'
                        +                                   '<li id="spclCharCnt" class="error">1 special character (Such as #, $, %, &, !)</li>'
                        +                               '</ul>'
                        +                           '</div>'
                        +                       '</li>'
                        +                       '<li class="input-field password">'
                        +                           '<p>'
                        +                               '<input autocomplete="off" type="password" name="registerCnfrmPwd" placeholder="Confirm Password" maxlength="14" />'
                        +                               '<span class="view-password"></span>'
                        +                           '</p>'
                        +                           '<div class="errorMsg"></div>'
                        +                       '</li>'
                        +                       '<li class="input-field ' + (loginType === 'email'? 'mobile-no': 'email') + '">'
                        +                           '<p>'
                        +                               (loginType === 'email'? '<span class="country-code">+91 - </span>': '')
                        +                               '<input autocomplete="off" type="text" name="' + (loginType === 'email'? 'mobile': 'emailId') + '" maxlength="' + (loginType === 'email'? '10': '100') + '" placeholder="' + (loginType === 'email'? 'Mobile Number': 'Email') + ' (Optional)" />'
                        +                           '</p>'
                        +                           '<div class="errorMsg"></div>'
                        +                       '</li>';
                        if(mod_login.showCaptcha()) {
                            registerScreen  +=  '<li class="recaptcha-wrapper">'
                                        +           '<div id="recaptcha-container"></div>'
                                        +           '<div class="errorMsg"></div>'
                                        +       '</li>';    
                        }

                        registerScreen +=       '<li class="checkbox">'
                        +                           '<p>'
                        +                               '<input type="checkbox" id="agree" name="agree" checked="checked">'
                        +                               '<label for="agree">I am at least 16 years old and agree with the'
                        +                               '<a href="https://www.indiatimes.com/termsandcondition/" target="_blank"> Terms & Conditions </a>and'
                        +                               '<a href="https://www.indiatimes.com/privacypolicy/" target="_blank"> Privacy Policy</a> of Times of India</label>'
                        +                           '</p>'
                        +                           '<div class="errorMsg"></div>'
                        +                       '</li>'
                        +                       '<li class="checkbox">'
                        +                           '<p>'
                        +                               '<input type="checkbox" id="promotions" name="promotions" checked="checked">'
                        +                               '<label for="promotions">Send me offers and promotions</label>'
                        +                           '</p>'
                        +                           '<div class="errorMsg"></div>'
                        +                       '</li>'
                        +                       '<li class="checkbox">'
                        +                           '<p>'
                        +                               '<input type="checkbox" id="sharedDataAllowed" name="sharedDataAllowed" checked="checked">'
                        +                               '<label for="sharedDataAllowed">Please show me personalized content and advertisements as per the '
                        +                               '<a href="https://www.indiatimes.com/privacypolicy/" target="_blank"> Privacy Policy</a></label>'
                        +                           '</p>'
                        +                           '<div class="errorMsg"></div>'
                        +                       '</li>'
                        +                       '<li class="submit">'
                        +                           '<input type="submit" id="sso-registerBtn" class="submit-btn" value="Update" />'
                        +                       '</li>'
                        +                   '</ul>'
                        +               '</form>'
                        +           '</div>'
                        +       '</div>';
        $('#signin_box').html(registerScreen);
        mod_login.setRecaptchaCode('');
        // Render racaptcha widget.
        if(typeof grecaptcha === 'object' && mod_login.showCaptcha()) {
            recaptchaWidgetId = grecaptcha.render(
                'recaptcha-container',
                {
                    "sitekey": "6LcXeh0TAAAAAO1DsEX1iEF8n8-E_hQB67bIpxIw", 
                    "theme": "light",
                    "callback": mod_login.recaptchaResponse,
                    "error-callback": mod_login.recaptchaErrorCallback,
                    "expired-callback": mod_login.recaptchaExpiredCallback
                }
            )
        }
    };
        
    /*Creates Forgot Password Screen UI and inserts in page*/
    mod_login.showForgotPasswordScreen = function (callback) {
        var inputVal = $('#lang_login input[name="emailId"]').val();
        var loginType = mod_login.getLoginType();
        var fpScreen = '';
        fpScreen +=  '<div id="lang_forgot_password">'
                    +       '<div class="signin-section">'
                    +           '<h4 class="heading">'
                    +               '<span>Forgot Password</span>'
                    +           '</h4>'
                    +           '<p id="forgot-password-sent">'
                    +               'We have sent a 6 digit verification code ' + (loginType !== 'email'? 'on <strong>+91-': 'to <strong>') + inputVal + '</strong>'
                    +           '</p>'
                    +           '<form class="form" autocomplete="off">'
                    +               '<input type="hidden" id="fp-inputVal" value="' + inputVal + '"/>'
                    +               '<ul>'
                    +                   '<li class="input-field password">'
                    +                       '<p>'
                    +                           '<input type="password" name="otpfp" maxlength="6" placeholder="Enter the verification code"/>'
                    +                       '</p>'
                    +                       '<div class="errorMsg"></div>'
                    +                       '<div class="successMsg"></div>'
                    +                       '<span class="regenerate-otp">Didn\'t receive OTP?</span>'
                    +                       '<a id="sso-fp-regenerate-otp" href="javascript:void(0)" class="secondary-link">Re-Generate OTP</a>'
                    +                   '</li>'
                    +                   '<li class="input-field password">'
                    +                       '<p>'
                    +                           '<input autocomplete="off" type="password" name="registerPwd" placeholder="Enter new password" maxlength="14" />'
                    +                           '<span class="view-password"></span>'
                    +                       '</p>'
                    +                           '<span class="subtext">Should not match last 3 passwords.</span>'
                    +                           '<div class="errorMsg"></div>'
                    +                           '<div class="password-conditions">'
                    +                               '<p>Password must have:</p>'
                    +                               '<ul>'
                    +                                   '<li id="charCnt" class="error">6-14 characters</li>'
                    +                                   '<li id="lwCnt" class="error">1 Lower case character (a-z)</li>'
                    +                                   '<li id="numCnt" class="error">1 Numeric character (0-9)</li>'
                    +                                   '<li id="spclCharCnt" class="error">1 special character (Such as #, $, %, &, !)</li>'
                    +                               '</ul>'
                    +                           '</div>'
                    +                   '</li>'
                    +                   '<li class="submit">'
                    +                       '<input type="submit" id="sso-fp-btn" class="submit-btn disabled" value="Verify & Login" disabled="disabled" />'
                    +                   '</li>'
                    +               '</ul>'
                    +           '</form>'
                    +       '</div>'
                    +   '</div>';
        $('#signin_box').html(fpScreen);
    };
        
    /**
     * Creates Verify OTP Screen UI to be shown after Register page and inserts in page
     * @param Mobile will be set when user tries to Register with mobile and email
     * @param emailId will be set once user verifies Mobile. Format email#mobile
     */
    mod_login.showSignUpOtpScreen = function (ssoid, mobile, emailId, callback) {
        var emailIdArr = emailId && emailId.length > 0? emailId.split('#'): [];
        var inputVal = emailIdArr[0] || $('#register-inputVal').val();
        var email = '';
        var pageName = '';
        var loginType = mod_login.getLoginType();
        if(loginType === 'email' && mobile && mobile.length) {
            pageName = 'mobile';
        } else if(emailIdArr && emailIdArr.length > 0) {
            pageName = 'email';
        } else if(loginType === 'email') {
            pageName = 'email';
        } else {
            pageName = 'mobile';
        }

        mod_login.setPageName(pageName);

        if(mobile && mobile.length) {
            loginType = 'mobile';
            email = inputVal;
            inputVal = mobile;
        }
        var fpScreen = '';
        fpScreen +=  '<div id="verifyotp-password">'
                    +       '<div class="signin-section">'
                    +           '<h4 class="heading">'
                    +               '<span>Complete Your Profile</span>'
                    +           '</h4>';
        if(emailIdArr.length > 0) {
            fpScreen    +=      '<p class="mn-verified">Mobile number verified: <strong>+91-' + emailIdArr[1] + '</strong><i class="tick"></i>'
                        +       '<p>Verify your email id</p>';
        } else {
            fpScreen    +=      '<p>We have sent a 6 digit verification code on your ' + (loginType !== 'email'? 'Mobile Number': 'Email Id') + '</p>';
        }

        fpScreen    +=          '<form class="form" autocomplete="off">'
                    +               '<input type="hidden" id="verify-inputVal" value="' + inputVal + '"/>'
                    +               '<input type="hidden" id="verify-email" value="' + email + '"/>'
                    +               '<input type="hidden" id="verify-ssoid" value="' + ssoid + '"/>'
                    +               '<input type="hidden" id="verify-logintype" value="' + loginType + '"/>'
                    +               '<ul>'
                    +                   '<li class="input-field ' + (loginType !== 'email'? 'mobile-no': 'email') + '">'
                    +                       '<p>'
                    +                           (loginType !== 'email'? '<span class="country-code">+91 - </span>': '')
                    +                           '<input autocomplete="off" type="text" name="verify-emailid" maxlength="100" disabled="disabled" value="' + inputVal  + '" />'
                    +                       '</p>'
                    +                       '<a href="javascript:void(0)" id="changeRegisterEmailId" class="secondary-link">Change Email/Mobile No.</a>'
                    +                   '</li>'
                    +                   '<li class="input-field password">'
                    +                       '<p>'
                    +                           '<input type="password" name="otpverify" maxlength="6" placeholder="Enter the verification code"/>'
                    +                       '</p>'
                    +                       '<div class="errorMsg"></div>'
                    +                       '<div class="successMsg"></div>'
                    +                       '<span class="regenerate-otp">Didn\'t receive OTP?</span>'
                    +                       '<a id="sso-verify-regenerate-otp" href="javascript:void(0)" class="secondary-link">Re-Generate OTP</a>'
                    +                   '</li>'
                    +                   '<li class="submit">'
                    +                       '<input type="submit" id="' + (emailId? 'sso-verify-email-btn': 'sso-verify-btn') + '" class="submit-btn disabled" value="' + (emailId? 'Verify': 'Verify and Login') + '" disabled="disabled" />'
                    +                   '</li>'
                    +               '</ul>'
                    +           '</form>'
                    +           '<div class="mandatory-box"><p>*Email or mobile no. verification is mandatory to complete the registration process.</p></div>'
                    +       '</div>'
                    +   '</div>';
        $('#signin_box').html(fpScreen);
    };
        
    mod_login.showSuccessMsgScreen = function (isForgotPassword, data) {
        var successScreen = '';
        successScreen   +=  '<div id="lang-success-screen">'
                        +       '<div class="signin-section">'
                        +           '<h4 class="heading">'
                        +               '<span>' + (isForgotPassword? 'Forgot Password': 'Complete Your Profile') + '</span>'
                        +           '</h4>';
        if(isForgotPassword) {
            successScreen   +=      '<div class="fp-success">'
                            +           '<i class=""></i>'
                            +           '<div class="fp-success-msg">Password changed successfully.</div>'
                            +       '</div>';
        } else {
            successScreen   +=      '<div class="register-success">'
                            +           '<div class="verified">'
                            +               '<div>' + (data.email? 'Email Id': 'Mobile Number') + ' verified:</div>'
                            +               '<div><strong>' + (data.email || data.mobile) + '</strong><i class="tick"></i></div>'
                            +           '</div>'
                            +           '<div class="success-wrapper">'
                            +               '<i class="success-user"></i>'
                            +               '<div class="fp-success-msg">Thank you for registering.</div>'
                            +           '</div>'
                            +       '</div>';
        }

        successScreen   +=      '</div>'
                        +   '</div>';

        $('#signin_box').html(successScreen);
        setTimeout(function(){
            var $loginPopUp = $("#login-popup");
            if($loginPopUp.hasClass('active')) {
                mod_login.closeBtnHandler();
            }
        }, 5000);
    };
        
    /**
     * Sets recaptcha code once it is validated
     * @param data - Recaptcha string returned in callback
     */
    mod_login.recaptchaResponse = function (data) {
        var $errorElement = $('li.recaptcha-wrapper');
        mod_login.handleError($errorElement);
        mod_login.setRecaptchaCode(data);
    };
    mod_login.recaptchaErrorCallback = function (err) {
        var $errorElement = $('li.recaptcha-wrapper');
        mod_login.handleError($errorElement, errorConfig.serverError);
    };
    mod_login.recaptchaExpiredCallback = function (data) {

    };
    /*Sets recaptcha code*/
    mod_login.setRecaptchaCode = function (data) {
        recaptchaCode = data;
    };
    /*returns recaptcha code*/
    mod_login.getRecaptchaCode = function () {
        return recaptchaCode;
    };
    mod_login.logout = function (callback) {
        loginCallback = function () {
            //event.publish("user.logout");
            if(typeof ga != "undefined")
            ga('send', 'event', 'uesr_logout', 'click', 'Logout');
            if (callback) {
                callback();
            }
        };
        reset();
        //mod_login.renderPlugins(false);
        var logout_url = constants.logoutUrl.replace('##$channel##', constants.channel); 
        var iframe = $('<iframe style="display:none;" height="0" />');
        iframe.attr('src', logout_url);  
        $(iframe).appendTo('body').on('load',function(){
            mod_login.removeUser();
            $(this).remove();
           mod_login.renderPlugins(false);
        }); 


        //localstorage.remove("sso_user");
        var domain = document.domain;
        var cookieList = [
            {name: 'ssoid', path: '/', domain: domain},
            {name: 'Fbsecuritykey', path: '/', domain: domain},
            {name: 'fbookname', path: '/', domain: domain},
            {name: 'CommLogP', path: '/', domain: domain},
            {name: 'CommLogU', path: '/', domain: domain},
            {name: 'FaceBookEmail', path: '/', domain: domain},
            {name: 'Fbimage', path: '/', domain: domain},
            {name: 'fbooklocation', path: '/', domain: domain},
            {name: 'Fboauthid', path: '/', domain: domain},
            {name: 'fbname', path: '/', domain: domain},
            {name: 'fbLocation', path: '/', domain: domain},
            {name: 'fbimage', path: '/', domain: domain},
            {name: 'fbOAuthId', path: '/', domain: domain},
            {name: 'MSCSAuth', path: '/', domain: domain},
            {name: 'MSCSAuthDetail', path: '/', domain: domain},
            {name: 'MSCSAuthDetails', path: '/', domain: domain},
            {name: 'Twimage', path: '/', domain: domain},
            {name: 'TwitterUserName', path: '/', domain: domain},
            {name: 'Twoauthid', path: '/', domain: domain},
            {name: 'Twsecuritykey', path: '/', domain: domain},
            {name: 'ssosigninsuccess', path: '/', domain: domain},
            {name: 'prc', path: '/', domain: domain},
            {name: 'jsso_crosswalk_ssec_lipi', path: '/', domain: domain},
            {name: 'jsso_crosswalk_tksec_lipi', path: '/', domain: domain},
            {name: 'jsso_crosswalk_tksec_nbt', path: '/', domain: domain},
            {name: 'jsso_crosswalk_tksec_nbt', path: '/', domain: domain},
            {name: 'ssoid'},
            {name: 'MSCSAuthDetail'},
            {name: 'articleid'},
            {name: 'txtmsg'},
            {name: 'tflocation'},
            {name: 'tfemail'},
            {name: 'setfocus'},
            {name: 'fbookname'},
            {name: 'CommLogP'},
            {name: 'CommLogU'},
            {name: 'FaceBookEmail'},
            {name: 'Fbimage'},
            {name: 'fbooklocation'},
            {name: 'Fboauthid'},
            {name: 'Fbsecuritykey'},
            {name: 'fbname'},
            {name: 'fbLocation'},
            {name: 'fbimage'},
            {name: 'fbOAuthId'},
            {name: 'MSCSAuth'},
            {name: 'MSCSAuthDetails'},
            {name: 'ssosigninsuccess'},
            {name: 'Twimage'},
            {name: 'TwitterUserName'},
            {name: 'Twoauthid'},
            {name: 'Twsecuritykey'},
            {name: 'prc'},
            {name: 'jsso_crosswalk_ssec_lipi'},
            {name: 'jsso_crosswalk_tksec_lipi'},
            {name: 'jsso_crosswalk_tksec_nbt'},
            {name: 'jsso_crosswalk_tksec_nbt'},
        ];


        for(var counter = 0 ; counter < cookieList.length; counter++) {
            if(cookieList[counter].path) {
                DelCookie(cookieList[counter].name, cookieList[counter].path, cookieList[counter].domain);
            } else {
                DelCookie(cookieList[counter].name);
            }
        };
    };
        var mod_login_config = {
            check_user_status: function (params, callback) {
                console.log('check_user_status12');
                var ssoid = Get_Ckie("ssoid") || Get_Ckie("ssoId");
                var MSCSAuthDetails = Get_Ckie("MSCSAuthDetails");
                    if (!ssoid && MSCSAuthDetails) {
                        ssoid = MSCSAuthDetails.split("=")[1];
                    }
                    console.log(MSCSAuthDetails,'check_user_status12');
                    console.log(ssoid,'check_user_status12');
                if (!MSCSAuthDetails && ssoid && ssoid.length > 0) {
                    //var isLoggedInUser = mod_login.getUser();
                   
                    if (callback) {
                        callback(ssoid);
                    }
                    //call API for MSCSAuthDetails by Ashish
                    var gerssogetTicket=tLoginObj.ssoUrl+'/sso/crossdomain/getTicket?version=v1';
                    $.ajax({
                        url : gerssogetTicket,
                        dataType : "jsonp",
                        success :function (data) {
                            if (data.ticketId != null && data.ticketId != undefined && data.ticketId.length > 0) {
                                var socialappurl=tLoginObj.socialappsintegrator+'/socialsite/v1validateTicket?ticketId=' + data.ticketId + '&channel='+langchannel;
                                $.ajax({
                                    url : socialappurl,
                                    dataType : "jsonp",
                                    success : function(d){
                                        console.log('use this data if you need to use this---');
                                        console.log(d);
                                        redirectAfterLogin();
                                    } 
                                });
                            }
                        } 
                    });                           
                } 
                else{
                    var gerssogetTicket=tLoginObj.ssoUrl+'/sso/crossdomain/getTicket?version=v1';
                    $.ajax({
                       url : gerssogetTicket,
                       dataType : "jsonp",
                        success :function (data) {
                            if (data.ticketId != null && data.ticketId != undefined && data.ticketId.length > 0) {
                                if (location.hostname.indexOf(".indiatimes.com") > -1) {
                                    
                                } 
                                else{
                                   // var socialappurl = tLoginObj.pluginsUrl+'/toi-t-login/v1validateTicket.php?ticketId=' + data.ticketId + '&channel='+langchannel;
                                    var socialappurl = tLoginObj.home_url+'/validatessoticket?ticketId=' + data.ticketId + '&channel='+langchannel;
                                    $.ajax({
                                        url : socialappurl,
                                        dataType : "jsonp",
                                        success : function(d){
                                            if(d.msg == 'success'){
                                                console.log(d.msg,'IKHWAN');
                                                mod_login.renderPlugins(true);
                                            }
                                        } 
                                    });
                                 }
                            }
                        }
                    });
                    if (callback) {
                        callback(ssoid);
                    }      
                }
             },
            mapping: {
                "uid": "uid",
                "email": "EMAIL", // map email
                "id": "_id",
                "name": "FL_N",
                "username": "D_N_U",
                "fullName": "FL_N",
                "firstName": "F_N",
                "lastName": "L_N",
                "icon": "tiny",
                "link": "profile",
                "CITY": "CITY",
                "thumb": "thumb",
                "followersCount": "F_C",
                "FE_C": "FE_C",
                "I_U_A": "I_U_A",
                "I_I_L": "I_I_L",
                "badges": "badges",
                "rewards": "rewards",
                "whatsonid": "W_ID",
                "ps": "SUB_U_J"
            }
        };
        mod_login.renderPlugins = function (user) {
                
            console.log('user=======', user);
            if (user) {
                var jssoObj = mod_login.setAndGetJssoCrosswalkObj();
                jssoObj.getUserDetails(function(response){
                    try {
                    var userObj = response.data;
                    
                    //console.log(response.data.firstName);
                    $("[data-plugin='user-isloggedin']").show();
                    $("[data-plugin='user-notloggedin']").hide();
                    $("[data-plugin='user-name']").text(userObj.firstName);
                    var userdp=userObj.dp!=null?userObj.dp:config.default_user_icon;
                    $("[data-plugin='profile-img']").attr("src", userdp);
                    
                        userInfoconfig.fstname=userObj.firstName;
                        userInfoconfig.fullname=userObj.firstName;
                        userInfoconfig.lstname=userObj.lastName;
                        userInfoconfig.email=userObj.emailList;
                        userInfoconfig.ssoId = userObj.ssoid;
                        userInfoconfig.D_N_U='';
                        userInfoconfig.sso=userObj.emailList;
                        userInfoconfig.city=userObj.city;
                        userInfoconfig.loggedstatus=1;
                        userInfoconfig._id='';
                        userInfoconfig.thumb=userObj.dp;
                        userInfoconfig.SIU='';
                        if(userObj.mobileList){
                            userInfoconfig.mobileList=userObj.mobileList;
                        }
                        window.localStorage.setItem('userInfo', JSON.stringify(userInfoconfig));
//                         
                      }catch(ex){
                            $("[data-plugin='profile-img']").attr("src", config.default_user_icon);
                            //$("[data-plugin='user-isloggedin']").hide();
                            $("[data-plugin='user-notloggedin']").show();
                      }
                }); 
            } else {
                $("[data-plugin='profile-img']").attr("src", config.default_user_icon);
                //$("[data-plugin='user-isloggedin']").hide();
                $("[data-plugin='user-notloggedin']").show();

            }
            $("body").toggleClass("loggedin", !!user);
            $("body").toggleClass("notloggedin", !user);
        };
        mod_login.register = function () {
            //logger.info("Register event called.");
            if(typeof ga != "undefined")
                ga('send', 'event', 'user_login', 'click', 'Register User');
        };
       
        mod_login.isLoggedIn = function (callback) {
            console.log('isLoggedIn');
            mod_login_config.check_user_status({},function (result) {
                
                console.log('isLoggedIn====');   
               console.log(result);
               console.log('isLoggedIn====12');   
               mod_login.renderPlugins(result);//result contains ssoid for now
                
            });
            
        };
        mod_login.removeUser = function (userId) {
            if (config.multiuser) {
                if (userId) {
                    delete userList[userId];
                } else {
                    throw new Error("'userId' is required to remove a user.");
                }
            } else {
                delete userList[single_user_id];
            }
            mod_login.statusChange(null);
        };
        mod_login.setUser = mod_login.addUser = function (_user) {
            if (typeof _user !== 'undefined' && !user.isUser(_user)) {
                throw new Error("Object is not an instance of User, use 'user.getNewUser()' to get a User object.");
            }
            if (config.multiuser) {
                userList[_user.id](_user);
            } else {
                userList[single_user_id] = _user;
            }
            mod_login.statusChange(_user);
        };
        mod_login.getUser = function (userId) {
            if (config.multiuser) {
                return $.extend(true, {}, userList[userId]);
            } else {
                return userList[single_user_id];
            }
        };
        mod_login.statusChange = function (user) {
            //event.publish("user.status", user);
            // Refresh Iframes with data-refreshState attr
            mod_login.refreshIframes();
        };

        mod_login.refreshIframes = function () {
            $(window.parent.document).find('iframe[data-refreshstate]').each(function (i, ele) {
                $(ele).attr('src', $(ele).attr('src'))
            })
        };
        mod_login.onStatusChange = function (callback) {  
            // event.subscribe("user.status", callback);
        };
        mod_login.updateConfig = function (init_config) {
            if (init_config) {
                config = $.extend(true, {}, config, init_config);
            }
        };
        
        /*Callback that calls forgot password API*/
        mod_login.forgotPasswordBtnHandler = function (e) {
            e.preventDefault();
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#fp-inputVal').val(),
                $fpScreen = $('#lang_forgot_password'),
                otp = $fpScreen.find('input[name="otpfp"]').val(),
                password = $fpScreen.find('input[name="registerPwd"]').val(),
                fnCall;
            
            fnCall = (loginType === 'email'? jssoObj.loginEmailForgotPassword: jssoObj.loginMobileForgotPassword);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, otp, password, password, mod_login.handleForgotPasswordVerifyCallback);
                mod_login.fireGAEvent(mod_login.getPageName() + '_PW_Verify');  
            }
        };
        
        /*Callback returned by Forgot password API with response*/
        mod_login.handleForgotPasswordVerifyCallback = function (response) {
            mod_login.hideLoader();
            var $errorElementOtp = $('#lang_forgot_password input[name="otpfp"]').closest('li');
            var $errorElementPass = $('#lang_forgot_password input[name="registerPwd"]').closest('li');
            var loginType = mod_login.getLoginType();
            if(response.code === 200) {
                // $('#signin_box-in').html('').hide();
                mod_login.fireGAEvent( 'Login_Success_' + mod_login.getPageName());
                // $('#login-popup .close-btn').click();
                mod_login.showSuccessMsgScreen(true);
                mod_login.isLoggedIn(loginCallback);
            } else {
                // Reset error and success messages
                mod_login.handleError($errorElementOtp);
                mod_login.handleError($errorElementPass);
                $('.successMsg').hide();
                switch(response.code) {
                    case 414: 
                        mod_login.handleError($errorElementOtp, (loginType === 'email'? errorConfig.wrongOtpEmail: errorConfig.wrongOtp));
                        break;
                    case 415:
                        mod_login.handleError($errorElementOtp, errorConfig.expiredOTP);
                        break;
                    case 416:
                        mod_login.handleError($errorElementOtp, errorConfig.limitExceeded);
                        break;
                    case 418:
                        mod_login.handleError($errorElementPass, errorConfig.matchLastThree);
                        break;
                    case 503:
                        mod_login.handleError($errorElementPass, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElementPass, errorConfig.serverError);
                        
                }
                mod_login.fireGAEvent('API_Error_' + response.code);
            } 
        };
        
        /*Click handler of Forgot password link on Login Screen.*/
        mod_login.forgotPasswordHandler = function (e) {
            if($('#sso-forgot-pass').hasClass('disabled')) {
                return;
            }
            var $emailId = $('#lang_login input[name="emailId"]');
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#lang_login input[name="emailId"]').val(),
                $errorElement = $('#lang_login li.email'),
                fnCall;
                
            
            if(inputVal.length === 0) {
                mod_login.handleError($errorElement, errorConfig.fpNoEmailOrMobile);
                return;
            } else if(!loginType) {
                mod_login.handleError($errorElement, errorConfig.fpInvalidEmail);
                return;
            }
            
            mod_login.handleError($errorElement);
            inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
            fnCall = (loginType === 'email'? jssoObj.getEmailForgotPasswordOtp: jssoObj.getMobileForgotPasswordOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, mod_login.handleForgotPasswordOTPCallback);
            }
            
            mod_login.setPageName(mod_login.getLoginType());
            mod_login.fireGAEvent(mod_login.getPageName() + '_Forgot_PW');
        };
        
        /*Sets error messages on screens
         * @param $errorElement - Parent element within which error messages have to be set
         * @param msg - Error message to be set
         */
        mod_login.handleError = function ($errorElement, msg) {
            if(msg) {
                $errorElement.find('p').addClass('error');
                $errorElement.find('.errorMsg').html(msg).show();
            } else {
                $errorElement.find('p').removeClass('error');
                $errorElement.find('.errorMsg').html('').hide();
            }
        };
        
        /*
         * Callback after sending OTP for Forgot password
         * @param response - Object
         * @param 
         */     
        mod_login.handleForgotPasswordOTPCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('#lang_login li.email');
            if(response && response.code === 200) {
                mod_login.showForgotPasswordScreen();
                var loginType = mod_login.getLoginType();
                mod_login.setScreenName('Forgot_PW');
            } else {
                if([405, 406, 407, 408].indexOf(response.code) !== -1) {
                    mod_login.handleError($errorElement, errorConfig.accountUnregistered);
                } else if(response.code === 503) {
                    mod_login.handleError($errorElement, errorConfig.connectionError);
                } else if (response.code === 416) {
                    mod_login.handleError($errorElement, errorConfig.limitExceeded);
                    $('#sso-regenerate-otp, #sso-generate-otp, #sso-forgot-pass').addClass('disabled');
                } else {
                    mod_login.handleError($errorElement, errorConfig.serverError);
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        /**
         * Keyup event handler for Forgot Password page
         * @param response - Object
         * @param 
         */
        mod_login.fpInputKeyupHandler = function (e) {
            var $this = $(this);
            // setTimeout required for paste events.
            setTimeout(function () {
                var $fpScreen = $('#lang_forgot_password');
                var otp = $fpScreen.find('input[name="otpfp"]').val();
                var password = $fpScreen.find('input[name="registerPwd"]').val();
                var $fbBtn = $('#sso-fp-btn');
                var enableFpBtn = true;
                // check if OTP is number and length is 6 and password is valid
                if(!(!isNaN(otp) && otp.length === 6) || !mod_login.isPasswordValid(password)) {
                    enableFpBtn = false;
                }
                
                $fbBtn.prop('disabled', !enableFpBtn);
            	if(enableFpBtn) {
            	    $fbBtn.removeClass('disabled');
            	} else {
            	    $fbBtn.addClass('disabled');
            	}
            	
            	// If keyup is for password field call password error function to handle its errors
            	if($this.attr('name') === 'registerPwd') {
            	    mod_login.passwordErrors.call($this, e);
            	}
            }, 0);
        };
        
        /*Handles Change Email/Mobile link click*/
        mod_login.changeEmailIdHandler = function (e) {
            $('#sso-pwdDiv, #changeEmailIdDiv, #sso-otpLoginDiv, #sso-login-otp-msg').hide();
            $('#lang_login input[name="emailId"]').prop('disabled', false).val('');
            $('#sso-signInButtonDiv input[type="submit"]').prop('disabled', true).addClass('disabled');
            $('.errorMsg, .successMsg').hide();
            $('.error').removeClass('error');
            $('#sso-signInButtonDiv > input').val('Continue');
            $('#sso-pwdDiv input[name="password"]').val('');
            $('#sso-otpLoginDiv input[type="password"]').val('');
            $('#sso-regenerate-otp, #sso-fp-regenerate-otp, #sso-verify-regenerate-otp, #sso-generate-otp, #sso-forgot-pass').removeClass('disabled');
            mod_login.fireGAEvent(mod_login.getPageName() + '_Change');
            mod_login.setScreenName('Login_Screen');
        };
        
        /*Shows login screen when user clicks change email on Register page*/
        mod_login.changeRegisterEmailIdHandler = function (e) {
            mod_login.showLoginScreen();
            mod_login.fireGAEvent( mod_login.getPageName() + '_Change');
        };
        
        /**Handles Email Id/ Mobile input field on Login page**/
        mod_login.handleEmailIdKeyUp = function (e) {
        	var $this = $(this);
        	setTimeout(function (e) {
        	    var val = $this.val(),
            	    checkIsEmail = val.indexOf('@'),
            	    checkIsMobile = !isNaN(val) && val.length >= 10,
            	    enableSignIn = false,
            	    $errorElement = $('#lang_login li.email'),
            	    $ssoSignInInputBtn = $('#sso-signInButtonDiv > input[type="submit"]');
            	    
            	if(checkIsEmail && mod_login.getValidEmailId(val).length > 0) {
            	    mod_login.setLoginType('email');
            	    enableSignIn = true;
            	} else if(checkIsMobile && mod_login.getValidMobileNumber(val).length > 0) {
            	    mod_login.setLoginType('mobile');
            	    enableSignIn = true;
            	} else {
            	    mod_login.setLoginType('');
            	}
            	
            	$ssoSignInInputBtn.prop('disabled', !enableSignIn);
            	mod_login.handleError($errorElement);
            	if(enableSignIn) {
            	    $ssoSignInInputBtn.removeClass('disabled');
            	} else {
            	    $ssoSignInInputBtn.addClass('disabled');
            	}
        	}, 0);
        };
        
        /*API callback of checkUserExists
         * @param response - Response object
         * @param 
         */
        mod_login.checkUserExists = function(response) {
            mod_login.hideLoader();
            var $errorElement = $('#lang_login li.email');
            var $emailId = $('#lang_login input[name="emailId"]');
            var errorMsg = '';
            var loginType = mod_login.getLoginType();
            mod_login.handleError($errorElement);
            console.log(response);
            if(response && response.code === 200 && response.data) {
                if(response.data.statusCode === 212 || response.data.statusCode === 213) {
                    $('#sso-pwdDiv, #changeEmailIdDiv').show();
                    $('#sso-signInButtonDiv > input').val('Sign In');
                } else if(response.data.statusCode === 205 || response.data.statusCode === 206 || response.data.statusCode === 214 || response.data.statusCode === 215) {
                    mod_login.registerUser();
                    mod_login.setScreenName('Register_New_User');
                } else {
                    $emailId.prop('disabled', false);
                    errorMsg = response.data.statusCode === 216 ? errorConfig.fpInvalidEmailOnly: errorConfig.fpInvalidEmail;
                    mod_login.handleError($errorElement, errorMsg);
                }
            } else {
                $emailId.prop('disabled', false);
                if(response.code === 410) {
                    mod_login.handleError($errorElement, (loginType === 'email'? errorConfig.fpInvalidEmailOnly: errorConfig.fpInvalidMobileOnly));
                } else if(response.code === 503) {
                    mod_login.handleError($errorElement, errorConfig.connectionError);
                } else {
                    mod_login.handleError($errorElement, errorConfig.serverError);
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        /*Shows register screen*/
        mod_login.registerUser = function() {
            mod_login.showRegisterScreen();
        };
        
        /*Handles Register button click and validates Register form*/
        mod_login.registerButtonHandler = function (e) {
            e.preventDefault();
            mod_login.registerFormSubmitted(true);
            var $register = $('#lang_register');
            var $email = $register.find('input[name="emailId"]');
            var $fullname = $register.find('input[name="fullname"]');
            var $password = $register.find('input[name="registerPwd"]');
            var $cnfrmPassword = $register.find('input[name="registerCnfrmPwd"]');
            var $mobile = $register.find('input[name="mobile"]');
            var recaptcha = mod_login.getRecaptchaCode();
            var $agree = $register.find('input[name="agree"]');
            var agree = $agree.is(':checked');
            var isSendOffer = $register.find('input[name="promotions"]').is(':checked');
            var sharedDataAllowed = $register.find('input[name="sharedDataAllowed"]').is(':checked')? '1': '0';
            var email = $email.val();
            var fullname = $fullname.val();
            var password = $password.val().trim();
            var cnfrmPassword = $cnfrmPassword.val().trim();
            var mobile = $mobile.val() || '';
            var username = {};
            var validForm = true;
            var loginType = mod_login.getLoginType();
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(), fnCall;
            var $fullNameParent = $fullname.closest('li');
            var $passwordParent = $password.closest('li');
            var $cnfrmPasswordParent = $cnfrmPassword.closest('li');
            var $mobileParent = $mobile.closest('li');
            var $emailParent = $email.closest('li');
            var $agreeParent = $agree.closest('li');
            var $recaptchaParent = $('#recaptcha-container').closest('li');
            
            var isValidName = mod_login.checkAndSetFullNameError($fullname, $fullNameParent);
            var isValidCnfrmPassword = mod_login.checkAndSetConfirmPasswordError($cnfrmPassword, $cnfrmPasswordParent);
            var isValidEmailOrMobile = true;
            var isTnCAgreed = mod_login.checkAndSetAgreeTnCError($agree, $agreeParent);
            
            if(loginType === 'email') {
                isValidEmailOrMobile = mod_login.checkAndSetEmailOrMobileToRegisterError($mobile, $mobileParent, 'mobile');
            } else {
                isValidEmailOrMobile = mod_login.checkAndSetEmailOrMobileToRegisterError($email, $emailParent, 'email');
            }
            
            if(!isValidName || !isValidCnfrmPassword || !isValidEmailOrMobile || !isTnCAgreed) {
                validForm = false;
            }
            
            if(!mod_login.isPasswordValid(password)) {
                validForm = false;
            }
            
            if(mod_login.showCaptcha()) {
                if(!recaptcha) {
                    validForm = false;
                    mod_login.handleError($recaptchaParent, errorConfig.captchaUnselected);
                } else {
                     mod_login.handleError($recaptchaParent);
                } 
            }
            
            $('.password-conditions').show();
            
            if(validForm) {
                username = mod_login.getFirstAndLastName(fullname);
                // Call registerUser API in case of Opera browser
                fnCall = mod_login.showCaptcha()? jssoObj.registerUserRecaptcha: jssoObj.registerUser;
                if(typeof fnCall === 'function') {
                    mod_login.showLoader();
                    if(mod_login.showCaptcha()) {
                        fnCall.call(jssoObj, username.firstName, username.lastName, '', '', email, mobile, password, isSendOffer, recaptcha, '1', sharedDataAllowed, mod_login.registerUserCallback);
                    } else {
                        fnCall.call(jssoObj, username.firstName, username.lastName, '', '', email, mobile, password, isSendOffer, '1', sharedDataAllowed, mod_login.registerUserCallback);
                    }
                }
            }
            
            mod_login.fireGAEvent( mod_login.getPageName() + '_Verify' );
        };
        
        /**Handles keyup events on register button*/
        mod_login.registerFormErrorHandler = function (e) {
            if(!registerFormSubmitted) {
                return;
            }
            
            var $inputElem = $(e.target);
            var inputFieldName = $inputElem.attr('name');
            var $elemParent = $inputElem.closest('li');
            
            if(inputFieldName === 'fullname') {
                mod_login.checkAndSetFullNameError($inputElem, $elemParent);
            } else if (inputFieldName === 'registerCnfrmPwd') {
                mod_login.checkAndSetConfirmPasswordError($inputElem, $elemParent);
            } else if (inputFieldName === 'emailId') {
                mod_login.checkAndSetEmailOrMobileToRegisterError($inputElem, $elemParent, 'email');
            } else if (inputFieldName === 'mobile') {
                mod_login.checkAndSetEmailOrMobileToRegisterError($inputElem, $elemParent, 'mobile');
            } else if (inputFieldName === 'agree') {
                mod_login.checkAndSetAgreeTnCError($inputElem, $elemParent);
            }
        };
        
        mod_login.checkAndSetFullNameError = function (inputElem, elemParent) {
            var nameRegex = /^[a-zA-Z\s]*$/;
            var fullname = inputElem.val();
            var validField = true;
            
            if(!(fullname && fullname.length > 0 && nameRegex.test(fullname))) {
                validField = false;
                if(fullname.length === 0) {
                    mod_login.handleError(elemParent, errorConfig.emptyName);
                } else {
                    mod_login.handleError(elemParent, errorConfig.wrongName);
                }
            } else {
                mod_login.handleError(elemParent);
            }
            
            return validField;
        };
        
        mod_login.checkAndSetConfirmPasswordError = function (inputElem, elemParent) {
            var password = $('#lang_register input[name="registerPwd"]').val().trim();
            var confirmPassword = inputElem.val().trim();
            var validField = true;
            if(password !== confirmPassword) {
                validField = false;
                mod_login.handleError(elemParent, errorConfig.passwordMismatch);
            } else {
                mod_login.handleError(elemParent);
            }
            
            return validField;
        };
        
        mod_login.checkAndSetEmailOrMobileToRegisterError = function (inputElem, elemParent, loginType) {
            var inputFieldVal = inputElem.val();
            var validField = true;
            
            if(inputFieldVal.length === 0 ) {
                mod_login.handleError(elemParent);
            } else {
                inputFieldVal = loginType === 'email'? mod_login.getValidEmailId(inputFieldVal): mod_login.getValidMobileNumber(inputFieldVal, true);
                if(inputFieldVal.length === 0) {
                    validField = false;
                    mod_login.handleError(elemParent, (loginType === 'email'? errorConfig.wrongEmail: errorConfig.wrongMobile));
                } else {
                    mod_login.handleError(elemParent);
                }
            }
            
            return validField;
        };
        
        mod_login.checkAndSetAgreeTnCError = function (inputElem, elemParent) {
            var tncAgreed = inputElem.is(':checked');
            var validField = true;
            if(!tncAgreed) {
                validField = false;
                mod_login.handleError(elemParent, errorConfig.tncNotSelected);
            } else {
                mod_login.handleError(elemParent);
            }
            return validField;
        };
        
        /**Handles Verify button click on Verify OTP page*/
        mod_login.verifyButtonHandler = function (e) {
            e.preventDefault();
            var $verifyParent = $('#verifyotp-password');
            var intent = $("#verify-inputVal").val();
            var ssoId = $("#verify-ssoid").val();
            var otp = $verifyParent.find('input[name="otpverify"]').val();
            var actualLoginType = mod_login.getLoginType();
            var loginType = $('#verify-logintype').val();
            var emailId = $('#verify-email').val() || '';
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj();
            var fnCall = (loginType === 'email'? jssoObj.verifyEmailSignUp: jssoObj.verifyMobileSignUp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, intent, ssoId, otp, mod_login.handleSignUpVerifyCallback((actualLoginType !== loginType && emailId? emailId: ''), ssoId));
                mod_login.fireGAEvent(mod_login.getPageName() + '_Verify');
            }
        };
        
        /*Handles Verify Email button click on Verify OTP page*/
        mod_login.verifyEmailButtonHandler = function (e) {
            e.preventDefault();
            // Duplicate method to handle any different functionality if any
            mod_login.verifyButtonHandler(e);
        };
        
        /**Enable Verify button of valid OTP is entered on Verify OTP page*/
        mod_login.enableVerifyButton = function (e) {
            var $this = $(this);
            setTimeout(function(){
                var otp = $this.val();
                var $submitBtn = $('#sso-verify-btn');
                if(!$submitBtn.is(':visible')) {
                    $submitBtn = $('#sso-verify-email-btn');
                }
                if(!isNaN(otp) && otp.length ===6 ) {
                    $submitBtn.prop('disabled', false).removeClass('disabled');
                } else {
                    $submitBtn.prop('disabled', true).addClass('disabled');
                }
            }, 0);
        };
        
        /**
         * Returns callback for Register User API
         * @param emailId, sso - Sets in case user tries to register with both email and mobile
         * @param response - API response
         */
        mod_login.handleSignUpVerifyCallback = function (emailId, sso, response) {
            console.log('handleSignUpVerifyCallback');
            return function (response) {
                mod_login.hideLoader();
                var $errorElementOtp = $('#verifyotp-password input[name="otpverify"]').closest('li');
                var loginType = $('#verify-logintype').val();
                var mobile = '';
                var $inputVal = $("#verify-inputVal");
                var verifiedData = {};
                if(response && response.code === 200) {
                    mod_login.fireGAEvent( 'Login_Success_' + mod_login.getPageName());
                    if(!emailId || !sso) {
                        if(loginType === 'email') {
                            verifiedData.email = $inputVal.val();
                        } else {
                            verifiedData.mobile = $inputVal.val();
                        }
                        mod_login.showSuccessMsgScreen(false, verifiedData);
                    } else {
                        mobile = $inputVal.val();
                        mod_login.showSignUpOtpScreen(sso, '', emailId + '#' + mobile);
                    }
                    
                    mod_login.isLoggedIn(loginCallback);
                } else {
                    $('.successMsg').hide();
                    switch(response.code) {
                        case 414: 
                            mod_login.handleError($errorElementOtp, (loginType === 'email'? errorConfig.wrongOtpEmail: errorConfig.wrongOtp));
                            break;
                        case 415:
                            mod_login.handleError($errorElementOtp, errorConfig.expiredOTP);
                            break;
                        case 416:
                            mod_login.handleError($errorElementOtp, errorConfig.limitExceeded);
                            break;
                        case 503:
                            mod_login.handleError($errorElementOtp, errorConfig.connectionError);
                            break;
                        default:
                            mod_login.handleError($errorElementOtp, (errorConfig.serverError));
                            
                    }
                    
                    mod_login.fireGAEvent('API_Error_' + response.code);
                }   
            }
        };
        
        
        mod_login.registerUserCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('#sharedDataAllowed').closest('li');
            var mobile = $('#lang_register input[name="mobile"]').val();
            if(response && response.code === 200) {
                mod_login.showSignUpOtpScreen(response.data.ssoid, mobile);
                mod_login.setScreenName('Complete_Profile');
            } else {
                if(response.code === 429) {
                    mod_login.handleError($errorElement, errorConfig.userAlreadyRegistered);
                } else if (response.code === 416) {
                    mod_login.handleError($errorElement, errorConfig.limitExceeded);
                } else if(response.code === 503) {
                    mod_login.handleError($errorElement, errorConfig.connectionError);
                } else {
                    mod_login.handleError($errorElement, errorConfig.serverError);
                }
                
                if(typeof grecaptcha === 'object' && mod_login.showCaptcha()) {
                    grecaptcha.reset(recaptchaWidgetId);
                }
                
                mod_login.setRecaptchaCode('');
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.getFirstAndLastName = function (name) {
            var nameArr = [];
            var nameObj = {firstName: '', lastName: ''};
            if(name && name.length > 0) {
                name = name.replace(/  +/g, ' ');
                nameArr = name.split(' ');
                nameObj.firstName = nameArr[0] || '';
                if(nameArr.length > 1) {
                    nameArr.splice(0,1);
                    nameObj.lastName = nameArr.join(' ');
                }
            }
            
            return nameObj;
        };
        
        mod_login.loginWithOTP = function(e, isRegenerate) {
            // Do not perform any action if generate otp is disabled
            if($('#sso-generate-otp').hasClass('disabled')) {
                return;
            }
            
            var $emailId = $('#lang_login input[name="emailId"]');
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#lang_login input[name="emailId"]').val(),
                fnCall;
            
            inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
            fnCall = (loginType === 'email'? jssoObj.getEmailLoginOtp: jssoObj.getMobileLoginOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, mod_login.handleLoginOTPCallback(isRegenerate));
                if(isRegenerate) {
                    mod_login.fireGAEvent( mod_login.getPageName() + '_Re_OTP');
                } else {
                    mod_login.fireGAEvent(mod_login.getPageName() + '_OTP_Submit');   
                }
            }
        };
        
        // Duplicate method in case any message needed for regenerate OTP logic        
        mod_login.regenerateLoginOTP = function() {
            // Do not perform any action if regenerate otp is disabled
            if($('#sso-regenerate-otp').hasClass('disabled')) {
                return;
            }
            
            mod_login.loginWithOTP({}, true);
        };
        
        mod_login.fpRegenerateOTP = function () {
            // Do not perform any action if regenerate OTP button is disabled
            if($(this).hasClass('disabled')) {
                return;
            }
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#fp-inputVal').val(),
                fnCall;
  
            fnCall = (loginType === 'email'? jssoObj.getEmailForgotPasswordOtp: jssoObj.getMobileForgotPasswordOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, mod_login.handleForgotPasswordRegenerateOTPCallback);
                mod_login.fireGAEvent(mod_login.getPageName() + '_Re_OTP');
            }
        };
        
        mod_login.handleForgotPasswordRegenerateOTPCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('input[name="otpfp"]').parent().parent();
            mod_login.handleError($errorElement);
            if(response && response.code === 200) {
                $('#lang_forgot_password input[name="otpfp"]').val('');
                $('.successMsg').text('OTP has been successfully sent.').show();
            } else {
                $('.successMsg').hide();
                switch(response.code) {
                    case 416:
                        mod_login.handleError($errorElement, errorConfig.limitExceeded);
                        $('#sso-fp-regenerate-otp').addClass('disabled');
                        $('#forgot-password-sent').hide();
                        break;
                    case 503:
                        mod_login.handleError($errorElement, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElement, errorConfig.serverError);
                    
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.verifyPageRegenerateOTP = function () {
            if($('#sso-verify-regenerate-otp').hasClass('disabled')) {
                return;
            }
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = $('#verify-logintype').val(),
                inputVal = $('#verify-inputVal').val(),
                ssoId = $('#verify-ssoid').val(),
                fnCall;
  
            fnCall = (loginType === 'email'? jssoObj.resendEmailSignUpOtp: jssoObj.resendMobileSignUpOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, ssoId, mod_login.handleSignUpVerifyRegenerateOTPCallback);
                mod_login.fireGAEvent(mod_login.getPageName() + '_Re_OTP');
            }
        };
        
        mod_login.handleSignUpVerifyRegenerateOTPCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('#verifyotp-password li.password:visible');
            mod_login.handleError($errorElement);
            if(response && response.code === 200) {
                $('#verifyotp-password input[name="otpverify"]').val('');
                $('.successMsg').text('OTP has been successfully sent.').show();
            } else {
                $('.successMsg').hide();
                switch(response.code) {
                    case 416:
                        mod_login.handleError($errorElement, errorConfig.limitExceeded);
                        $('#sso-verify-regenerate-otp').addClass('disabled');
                        break;
                    case 503:
                        mod_login.handleError($errorElement, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElement, errorConfig.serverError);
                    
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.handleLoginOTPCallback = function(isRegenerate) {
            return function (response) {
                mod_login.hideLoader();
                var $errorElement = $('#lang_login li.password:visible');
                mod_login.handleError($errorElement);
                if(response && response.code === 200) {
                    var loginType = mod_login.getLoginType();
                    var inputVal = $('#lang_login input[name="emailId"]').val();
                    inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
                    $('#sso-pwdDiv').hide();
                    $('#sso-otpLoginDiv, #sso-login-otp-msg').show();
                    $('#sso-login-otp-msg > p').text('We have sent a 6 digit verification code ' + (loginType === 'email'? 'to ': 'on +91-') + inputVal);
                    if(isRegenerate) {
                        $('#lang_login input[name="otplogin"]').val('');
                        $('#sso-otpLoginDiv .successMsg').text('OTP has been successfully sent.').show();
                    }
                } else {
                    $('#sso-otpLoginDiv .successMsg').hide();
                    switch(response.code) {
                        case 416:
                            mod_login.handleError($errorElement, errorConfig.limitExceeded);
                            // Disable Regenerate OTP button and remove text message specifying OTP has been sent
                            $('#sso-regenerate-otp, #sso-generate-otp, #sso-forgot-pass').addClass('disabled');
                            $('#sso-login-otp-msg > p').text('');
                            break;
                        case 503:
                            mod_login.handleError($errorElement, errorConfig.connectionError);
                            break;
                        default:
                            mod_login.handleError($errorElement, errorConfig.serverError);
                        
                    }
                    
                    mod_login.fireGAEvent('API_Error_' + response.code);
                }
            }
        };
        
        mod_login.fbLoginHandler = function (e) {
            var callback = function () {
                mod_login.closeBtnHandler();
            };
            
            mod_login.initiateFbLogin(callback);
        };
        
        mod_login.gplusLoginHandler = function () {
            var callback = function () {
                mod_login.closeBtnHandler();
            };
            
            mod_login.initiateGplusLogin(callback);
        };
        
        mod_login.handleLoginCallback = function (response) {
            console.log('handleLoginCallback');
            console.log(response);
            console.log('handleLoginCallback=');
            mod_login.hideLoader();
            var isOtpDivVisible = $('#sso-otpLoginDiv').is(':visible');
            var $errorElement = $('#lang_login li.password:visible');
            var loginType = mod_login.getLoginType();
            if(response && response.code === 200) {
                mod_login.closeBtnHandler();
                console.log('loginCallback');
                console.log(loginCallback);
                console.log('loginCallback===');
                mod_login.isLoggedIn(loginCallback);
                mod_login.fireGAEvent('Login_Success_' + mod_login.getPageName());
            } else {
                $('.successMsg').hide();
                switch(response.code) {
                    case 415:
                        mod_login.handleError($errorElement, (!isOtpDivVisible? errorConfig.wrongPassword: errorConfig.expiredOTP));
                        break;
                    case 416:
                        mod_login.handleError($errorElement, errorConfig.limitExceeded);
                        break;
                    case 503:
                        mod_login.handleError($errorElement, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElement, (!isOtpDivVisible? errorConfig.wrongPassword: (loginType === 'email'? errorConfig.wrongOtpEmail: errorConfig.wrongOtp )));
                        
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.handleEmailIdClick = function (e) {
            console.log('handleEmailIdClick');
            e.preventDefault();
            var $password = $('#sso-pwdDiv input[name="password"]');
            var $otp = $('#sso-otpLoginDiv input[type="password"]');
            var $emailId = $('#lang_login input[name="emailId"]');
            var password = '';
            var $errorMsgElem = $('#lang_login li.password:visible .errorMsg');
            $emailId.prop('disabled', true);
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#lang_login input[name="emailId"]').val(),
                fnCall;
            
            inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
            
            if($password.is(':visible') || $otp.is(':visible')) {
                fnCall = (loginType === 'email'? jssoObj.verifyEmailLogin: jssoObj.verifyMobileLogin);
                password = $password.is(':visible')? $password.val(): $otp.val();
                mod_login.fireGAEvent(mod_login.getPageName() + ($password.is(':visible')? '_PW': '_OTP') + '_Entry');
                if(password.length === 0) {
                    $errorMsgElem.html(errorConfig.emptyPassword).show();
                    return;
                } else if(typeof fnCall === 'function') {
                    $('.errorMsg').html('').hide();
                    mod_login.showLoader();
                    fnCall.call(jssoObj, inputVal, password, mod_login.handleLoginCallback);
                }
            } else {
                if(typeof jssoObj.checkUserExists === 'function') {
                    mod_login.showLoader();
                    jssoObj.checkUserExists(inputVal, mod_login.checkUserExists);
                    mod_login.setPageName(loginType);
                    mod_login.fireGAEvent(mod_login.getPageName() + '_Continue');
                } else {
                    $emailId.prop('disabled', false);
                }
            }
        };
        
        mod_login.getValidEmailId = function (email) {
            var regEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,6})$/;
            var emailId = '';
            if(regEmail.test(email)) {
                emailId = email;
            }
            
            return emailId;
        };
        
        // withoutPrefix parameter is needed to check for valid number without +91 or 0 appended
        mod_login.getValidMobileNumber = function (mobile, withoutPrefix) {
            var regMobile = (withoutPrefix? /^[789]\d{9}$/ : /^(\+91)?[0]?[789]\d{9}$/);
            var notAllowedNumbers = ['7777777777', '8888888888', '9999999999'];
            var mobileNo = '';
            var length = mobile.length;
            if(regMobile.test(mobile)) {
                mobileNo = mobile.substring(mobile.length - 10, mobile.length);
            }
            
            if(notAllowedNumbers.indexOf(mobileNo) !== -1) {
                mobileNo = '';
            }
            
            return mobileNo;
        };
        
        mod_login.closeModalOnEscapeKeyPress = function (e) {
            if(!$('#login-popup').hasClass('active')) {
                return;
            }
        	var keyCode = e.keyCode || e.which;
        	if(keyCode === 27) {
        		cachedElements.closeBtn.click();
        	}
        };
        
        mod_login.isPasswordValid = function (password){
        	return password && password.length >= 6 && password.length <= 14 && mod_login.hasNumber(password) && mod_login.hasSpecialCharacters(password) && mod_login.hasLowerCase(password);
        };
        
        mod_login.hasLowerCase = function (str) {
            return (/[a-z]/.test(str));
        };
        
        mod_login.hasNumber = function (str) {
            return (/[0-9]/.test(str));
        };
        
        mod_login.hasSpecialCharacters = function (str) {
            return (/[!@#$%^&*()]/.test(str));
        };
        
        mod_login.passwordErrors = function (e) {
            var $this = $(this);
            setTimeout(function(){
                var password= $this.val();
            	if(password.length < 6 || password.length > 14){
            		$("#charCnt").removeClass('success').addClass('error');
            	}else{
            		$("#charCnt").removeClass('error').addClass('success');
            	}
            	if(mod_login.hasLowerCase(password)){
            		$("#lwCnt").removeClass('error').addClass('success');
            	}else{
            		$("#lwCnt").removeClass('success').addClass('error');
            	}
            	if(mod_login.hasNumber(password)){
            		$("#numCnt").removeClass('error').addClass('success');
            	}else{
            		$("#numCnt").removeClass('success').addClass('error');
            	}
            	if(mod_login.hasSpecialCharacters(password)){
            		$("#spclCharCnt").removeClass('error').addClass('success');
            	}else{
            		$("#spclCharCnt").removeClass('success').addClass('error');
            	}
            }, 0);
        	
        	// return validPassword;
        };
        
        mod_login.showPassword = function (e) {
            var $this = $(this);
            $this.prev().attr('type', 'text');
            $this.removeClass('view-password').addClass('hide-password');
        };
        
        mod_login.hidePassword = function (e) {
            var $this = $(this);
            $this.prev().attr('type', 'password');
            $this.removeClass('hide-password').addClass('view-password');
        };
        
        mod_login.showPasswordCondition = function (e) {
            e.stopPropagation();
            $('.password-conditions').show();
        };
        
        mod_login.stopEventProp = function (e) {
            e.stopPropagation();  
        };
        
        mod_login.setLoginType = function (type) {
            ssoLoginType = type;
        }
        
        mod_login.getLoginType = function () {
            return ssoLoginType;
        }
        
        mod_login.setPageName = function(loginType) {
            pageName = (loginType === 'email'? 'Email': 'MobNo');
        }
        
        mod_login.getPageName = function() {
            return pageName;
        }
        
        mod_login.setScreenName = function (name) {
            screenName = name;
        };
        
        mod_login.getScreenName = function (name) {
            return screenName;
        };
        
        mod_login.setAndGetJssoCrosswalkObj = function () {
            var jssoObj = {};
            if (typeof JssoCrosswalk === 'function') {
                jssoCrosswalkObj = new JssoCrosswalk(langchannel, 'web');
                jssoObj = jssoCrosswalkObj;
            }
            
            return jssoObj;
        }
        
        mod_login.showLoader = function() {
            $('#signin_box').addClass('loader');
        }
        
        mod_login.hideLoader = function() {
            $('#signin_box').removeClass('loader');
        }
        
        mod_login.closeBtnHandler = function() {
            $('#login-popup').removeClass('active');
            $('body').removeClass('disable-scroll');
            if(typeof grecaptcha === 'object' && $('#lang_register').is(':visible') && mod_login.showCaptcha()) {
                grecaptcha.reset(recaptchaWidgetId);   
            }
        };
        
        mod_login.init = function (init_config) {
            var initCallback = function() {
                $('#login-popup').show();
                mod_login.updateConfig(init_config);
                /*if (config.renderer === true) {
                    mod_login.onStatusChange(function (_user) {
                        mod_login.renderPlugins(_user);
                    });
                }*/
                console.log('initCallback');
                mod_login.isLoggedIn(function () {
                });
                mod_login.initActions();
            }
            initCallback();
            //TimesApps.checkGdprAndCall(initCallback, mod_login.logout);
        };
        
       
        
        mod_login.initActions = function () {
            $("#login-popup .close-btn").on("click", function() {
                    mod_login.closeBtnHandler();
                    //mod_login.fireGAEvent('Close');
                });
            
            $("[data-plugin='user-isloggedin']")
                .on("click", "[data-plugin='user-logout']", function () {
                    mod_login.logout();
//                    mod_login.logout(function () {
//                        window.localStorage.removeItem("userInfo");
//                        document.getElementById('logout-form').submit(); 
//                        });
                    setTimeout(function(){ 
                        window.localStorage.removeItem("userInfo");
                            document.getElementById('logout-form').submit(); 
                    }, 2000);
                    
                });
            $("[data-plugin='user-notloggedin']")
                .on("click", "[data-plugin='user-login']", function () {
                    $('body').addClass('disable-scroll');
                    $('#login-popup').addClass('active');
                    mod_login.showLoginScreen();
                    mod_login.setScreenName('Login_Screen');
                   // mod_login.fireGAEvent('Load');
                })
                .on("click", "[data-plugin='user-register']", function () {
                    mod_login.register();
                })
                .on("click", "[data-plugin='user-login-facebook']", function () {
                    mod_login.loginWithFacebook();
                })
                .on("click", "[data-plugin='user-login-twitter']", function () {
                    mod_login.loginWithTwitter();
                })
                .on("click", "[data-plugin='user-login-google']", function () {
                    mod_login.loginWithGoogle();
                });
                
            $(document).off('keyup').on('keyup', mod_login.closeModalOnEscapeKeyPress);
            $("#signin_box")
                .off('keyup paste', '#lang_login input[name="emailId"]').on('keyup paste', '#lang_login input[name="emailId"]', mod_login.handleEmailIdKeyUp)
                .off('click', '#sso-signInButtonDiv input[type="submit"]').on('click', '#sso-signInButtonDiv input[type="submit"]', mod_login.handleEmailIdClick)
                .off('submit', '#lang_login form').on('submit', '#lang_login form', mod_login.handleEmailIdClick)
                .off('click', '#changeEmailIdDiv').on('click', '#changeEmailIdDiv', mod_login.changeEmailIdHandler)
                .off('click', '#changeRegisterEmailId').on('click', '#changeRegisterEmailId', mod_login.changeRegisterEmailIdHandler)
                .off('click', '#sso-forgot-pass').on('click', '#sso-forgot-pass', mod_login.forgotPasswordHandler)
                .off('click', '#sso-fb-login').on('click', '#sso-fb-login', mod_login.fbLoginHandler)
                .off('click', '#sso-gplus-login').on('click', '#sso-gplus-login', mod_login.gplusLoginHandler)
                .off('click', '#sso-generate-otp').on('click', '#sso-generate-otp', mod_login.loginWithOTP)
                .off('click', '#sso-regenerate-otp').on('click', '#sso-regenerate-otp', mod_login.regenerateLoginOTP)
                .off('click', '#sso-fp-regenerate-otp').on('click', '#sso-fp-regenerate-otp', mod_login.fpRegenerateOTP)
                .off('click', '#sso-verify-regenerate-otp').on('click', '#sso-verify-regenerate-otp', mod_login.verifyPageRegenerateOTP)
                .off('click', '#sso-registerBtn').on('click', '#sso-registerBtn', mod_login.registerButtonHandler)
                .off('submit', '#lang_register form').on('submit', '#lang_register form', mod_login.registerButtonHandler)
                .off('click', '#sso-verify-btn').on('click', '#sso-verify-btn', mod_login.verifyButtonHandler)
                .off('click', '#sso-verify-email-btn').on('click', '#sso-verify-email-btn', mod_login.verifyEmailButtonHandler)
                .off('submit', '#verifyotp-password form').on('submit', '#verifyotp-password form', mod_login.verifyButtonHandler)
                .off('click', '#sso-fp-btn').on('click', '#sso-fp-btn', mod_login.forgotPasswordBtnHandler)
                .off('submit', '#lang_forgot_password form').on('submit', '#lang_forgot_password form', mod_login.forgotPasswordBtnHandler)
                .off('focus', 'input[name="registerPwd"]').on('focus', 'input[name="registerPwd"]', mod_login.showPasswordCondition)
                .off('keyup paste', '#lang_register input[name="registerPwd"]').on('keyup paste', '#lang_register input[name="registerPwd"]', mod_login.passwordErrors)
                .off('keyup paste', '#lang_register input[type="text"]').on('keyup paste', '#lang_register input[type="text"]', mod_login.registerFormErrorHandler)
                .off('keyup paste', '#lang_register input[name!="registerPwd"][type="password"]').on('keyup paste', '#lang_register input[name!="registerPwd"][type="password"]', mod_login.registerFormErrorHandler)
                .off('change', '#lang_register input[name="agree"]').on('change', '#lang_register input[name="agree"]', mod_login.registerFormErrorHandler)
                // .off('focus blur', '[placeholder]:not(input[name="registerPwd"])').on('focus blur', '[placeholder]:not(input[name="registerPwd"])', mod_login.stopEventProp)
                .off('focus blur', '[placeholder]').on('focus blur', '[placeholder]', mod_login.stopEventProp)
                .off('keyup paste', '#lang_forgot_password input').on('keyup paste', '#lang_forgot_password input', mod_login.fpInputKeyupHandler)
                .off('keyup paste', '#verifyotp-password input[name="otpverify"]').on('keyup paste', '#verifyotp-password input[name="otpverify"]', mod_login.enableVerifyButton)
                .off('click', '.view-password').on('click', '.view-password', mod_login.showPassword)
                .off('click', '.hide-password').on('click', '.hide-password', mod_login.hidePassword)
        };
        if (config.autoinit === true) {
            mod_login.init();
        }
        
        $('#login-popup').off('click').on('click', function(e){
           if(e.target && e.target.id === 'login-popup') {
               cachedElements.closeBtn.click();
           }
        });
        mod_login.updateConfig(mod_login_config);
        return mod_login;
})();
	
	
function SetCookie_nbt(sName,sValue){
    document.cookie = sName + "=" + escape(sValue);
}
        
function GetCookie_nbt(sName){				  
    var start=document.cookie.indexOf(sName+"=")
    var len=start+sName.length+1
    if((!start)&&(sName !=document.cookie.substring(0,sName.length))){
            return null
    }
    if(start==-1)return null;
    var end=document.cookie.indexOf(";",len)
    if(end==-1)end=document.cookie.length
    return unescape(document.cookie.substring(len,end))
}
        
function DelCookie(name,path,domain){
    if(GetCookie_nbt(name))document.cookie=name+"="+((path)? ";path="+path : "")+((domain)? ";domain="+domain : "")+";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

function redirectAfterLogin(){
    var userData = JSON.parse(window.localStorage.getItem('userInfo'));
    var ssoid ='';
    var email='';
    var mobile='';
    var ssoid = Get_Ckie("ssoid") || Get_Ckie("ssoId");                   
    var MSCSAuthDetails = Get_Ckie("MSCSAuthDetails");
    if (MSCSAuthDetails) {
        email = MSCSAuthDetails.split("=")[1];
    }
    var jssoObj = {};
    if (typeof JssoCrosswalk === 'function') {
        jssoCrosswalkObj = new JssoCrosswalk(langchannel, 'web');
        jssoObj = jssoCrosswalkObj;
    }
    jssoObj.getUserDetails(function (response) {
        try {
            
            var userObj = response.data;
            var ssoUserInfo = {};
            ssoUserInfo.fname = userObj.firstName;            
            ssoUserInfo.lname = userObj.lastName;
            ssoUserInfo.ssoid = userObj.ssoid;
            if(userObj.emailList){
                var emailObj =Object.keys(userObj.emailList);
                ssoUserInfo.email = emailObj[0];
                if(emailObj[0]){
                    ssoUserInfo.email = emailObj[0];
                }else{
                    ssoUserInfo.email = '';
                } 
            }
            else{
                ssoUserInfo.email = '';
            }
            if (userObj.mobileList) {
                var mobileObj =Object.keys(userObj.mobileList);
                if(mobileObj[0]){
                    ssoUserInfo.mobile = mobileObj[0];
                }else{
                    ssoUserInfo.mobile = '';
                }                
            }else{
                ssoUserInfo.mobile = '';
            }
            //console.log(ssoUserInfo,'redirectAfterLogin');
            if(ssoUserInfo){
                var postData = encodeURIComponent(window.btoa(JSON.stringify(ssoUserInfo)));
                $.ajax({
                    type: 'POST',
                    data: {'pl':postData},
                    url: tLoginObj.home_url + "/checkUser",
                    dataType: "json",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        console.log(data, 'IKH');
                        if (data.status == true) {
                            if(data.code==202 && data.message=='Pending' && data.ru=='PAF'){
                               window.location.href = tLoginObj.home_url + "/paymentdetail";
                            }else if(data.code==200 && data.message=='Success' && data.ru=='BRD'){
                                 window.location.href = tLoginObj.home_url + "/dashboard";
                            }else if(data.code==201 && data.message=='Created' && data.ru=='PRF'){
                                window.location.href = tLoginObj.home_url + "/profileform";
                            }

                        } else {
                            window.location.href = tLoginObj.home_url+ "/404";;
                        }

                    }
                });
            }

        } catch (ex) {
            console.log('mmmmmmmmmmmmm');
            $("[data-plugin='user-notloggedin']").show();
        }
    }); 
    
    if(email || ssoid){
        
    }
    
}


function removeUserFromCookie(domainName){
    var domain = domainName;
    //alert(domain);
        var cookieList = [
            {name: 'ssoid', path: '/', domain: domain},
            {name: 'Fbsecuritykey', path: '/', domain: domain},
            {name: 'fbookname', path: '/', domain: domain},
            {name: 'CommLogP', path: '/', domain: domain},
            {name: 'CommLogU', path: '/', domain: domain},
            {name: 'FaceBookEmail', path: '/', domain: domain},
            {name: 'Fbimage', path: '/', domain: domain},
            {name: 'fbooklocation', path: '/', domain: domain},
            {name: 'Fboauthid', path: '/', domain: domain},
            {name: 'fbname', path: '/', domain: domain},
            {name: 'fbLocation', path: '/', domain: domain},
            {name: 'fbimage', path: '/', domain: domain},
            {name: 'fbOAuthId', path: '/', domain: domain},
            {name: 'MSCSAuth', path: '/', domain: domain},
            {name: 'MSCSAuthDetail', path: '/', domain: domain},
            {name: 'MSCSAuthDetails', path: '/', domain: domain},
            {name: 'Twimage', path: '/', domain: domain},
            {name: 'TwitterUserName', path: '/', domain: domain},
            {name: 'Twoauthid', path: '/', domain: domain},
            {name: 'Twsecuritykey', path: '/', domain: domain},
            {name: 'ssosigninsuccess', path: '/', domain: domain},
            {name: 'prc', path: '/', domain: domain},
            {name: 'jsso_crosswalk_ssec_lipi', path: '/', domain: domain},
            {name: 'jsso_crosswalk_tksec_lipi', path: '/', domain: domain},
            {name: 'jsso_crosswalk_tksec_nbt', path: '/', domain: domain},
            {name: 'jsso_crosswalk_tksec_nbt', path: '/', domain: domain},
            {name: 'lgc_ssoid', path: '/', domain: domain},
            {name: 'ssoid'},
            {name: 'MSCSAuthDetail'},
            {name: 'articleid'},
            {name: 'txtmsg'},
            {name: 'tflocation'},
            {name: 'tfemail'},
            {name: 'setfocus'},
            {name: 'fbookname'},
            {name: 'CommLogP'},
            {name: 'CommLogU'},
            {name: 'FaceBookEmail'},
            {name: 'Fbimage'},
            {name: 'fbooklocation'},
            {name: 'Fboauthid'},
            {name: 'Fbsecuritykey'},
            {name: 'fbname'},
            {name: 'fbLocation'},
            {name: 'fbimage'},
            {name: 'fbOAuthId'},
            {name: 'MSCSAuth'},
            {name: 'MSCSAuthDetails'},
            {name: 'ssosigninsuccess'},
            {name: 'Twimage'},
            {name: 'TwitterUserName'},
            {name: 'Twoauthid'},
            {name: 'Twsecuritykey'},
            {name: 'prc'},
            {name: 'jsso_crosswalk_ssec_lipi'},
            {name: 'jsso_crosswalk_tksec_lipi'},
            {name: 'jsso_crosswalk_tksec_nbt'},
            {name: 'jsso_crosswalk_tksec_nbt'},
            {name: 'lgc_ssoid'},
        ];


        for(var counter = 0 ; counter < cookieList.length; counter++) {
            if(cookieList[counter].path) {
                DelCookie(cookieList[counter].name, cookieList[counter].path, cookieList[counter].domain);
            } else {
                DelCookie(cookieList[counter].name);
            }
        };
        window.localStorage.removeItem("userInfo");
}

function getCookieSsoId() {
    var isSSOLogin = GetCookie_nbt('ssoid');
    console.log(isSSOLogin,'IAM');
    if (isSSOLogin) {
        //mod_login.logout();
        var appUrl = tLoginObj.home_url;
        appUrl = appUrl.replace('http://', '')
        appUrl = appUrl.replace('https://', '')
        console.log(appUrl,'IAM');
        removeUserFromCookie('.'+tLoginObj.domainName);
        removeUserFromCookie(appUrl);
         console.log(isSSOLogin,'IAM');
        //location.reload(true);
        //window.localStorage.removeItem("userInfo");
        document.getElementById('logout-form').submit(); 
    }
    
}





