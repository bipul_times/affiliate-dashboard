<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */



Route::group(array('domain' => env('ADMIN_DOMAIN')), function()
{
    Route::get('/', function() {
        if(Auth::check()){
            return view('layouts.admin.admin'); // admin login
        }else{
            return view('layouts.admin.login'); 
        }
    });
});

Route::get('/', function () {    
    if(Auth::check()){
        $user = Auth::user();
        //dd($user->status);
        if ($user->status == 'approved') {
            if ($user->hasRole('author')) {
                return redirect()->route('dashboard');
            }
        } else if ($user->status == 'pending') {
            return redirect()->route('payment.form');
        }
      
    }else{

        return view('welcome');
    }
});

Auth::routes();


Route::get('health-check', function () {
    return 'ok';
});

Route::get('logout', 'Auth\LoginController@logout');

Route::get('t-login-sso', 'UserController@ssoLogin')->name('t-login-sso');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('validatessoticket', 'UserController@validateSsoTicket')->name('validate.Ticket');
//Route::get('t-login-sso', 'HomeController@ssoLogin')->name('t-login-sso');
Route::post('checkUser', 'UserController@checkUser');
Route::get('profileform', 'UserController@userProfileForm')->name('profile.form');
Route::post('saveprofile', 'UserController@saveUserProfile')->name('profile.save');



Route::get('dropdownlist', 'DropdownController@index');
Route::get('get-state-list', 'DropdownController@getStateList');
Route::get('get-city-list', 'DropdownController@getCityList');
Route::get('get-country-list', 'DropdownController@getCountryList');
Route::get('getallindiacitylist', 'DropdownController@getAllIndiaCityList');
Route::get('getpricelist', 'DropdownController@getPriceType');
Route::get('getsubcategorylist', 'DropdownController@getSubCategoryList')->name('getsubcategorylist');

Route::get('view-image/{image_path?}', 'AuthorController@viewImage')->name('view-image');
Route::get('demo/{asset_type}/{asset_name}', 'UserController@registrationProcessDemo')->name('demo');

Route::middleware('auth')->group(function () {
    Route::get('paymentdetail', 'AuthorController@userPaymentForm')->name('payment.form');
    Route::post('save', 'AuthorController@saveUserPaymentDetail')->name('payment.save');
    Route::get('bankdetail', 'AuthorController@getBankDetail')->name('bank.detail');
    Route::get('profile', 'AuthorController@viewMyInformation')->name('viewmyinfo');
    Route::post('updateuserinfo', 'UserDetailReviewController@updateUserInfo')->name('update-userinfo');
    Route::post('paymentupdate', 'UserDetailReviewController@updateUserPaymentInfo')->name('payment.update');
});
Route::middleware('auth', 'role:author')->group(function () {

    Route::get('dashboard', 'AssignmentController@actionDashboard')->name('dashboard');
    Route::get('assignmentstatus', 'AssignmentController@userAssignmentStatus')->name('assignment-status');
    Route::get('jobs', 'AssignmentController@showOpenJobs')->name('jobs');
    Route::get('applyjob', 'AssignmentController@applyJobs')->name('job.apply');
    Route::get('myopenjobs', 'AssignmentController@getMyJobs')->name('myopenjobs');
    Route::get('assignmentform/{id}', 'UserAssignmentSubmissionController@writeAssignmentForm')->name('write.assignment');
    Route::Post('saveassignment', 'UserAssignmentSubmissionController@saveWriterAssignment')->name('save.writerassignment');
    Route::get('deleteassignmentimage', 'UserAssignmentSubmissionController@deleteAssignmentImage')->name('delete.assignmentimage');
    Route::get('editors-choice', 'AssignmentInvitationController@index')->name('editors-choice');
    Route::get('editors-choice/{id}/{action}/{AI_ID}', 'AssignmentInvitationController@action')->name('editors-choice-action');

    
    Route::get('payment', 'AuthorController@viewMyPaymentInformation')->name('viewmypaymentinfo');
    Route::get('my-invoice-list', 'UserAssignmentSubmissionController@myInvoiceList')->name('my-invoice-list');
    Route::get('download-invoice/{id}', 'UserAssignmentSubmissionController@downloadInvoice')->name('download-invoice');
    Route::get('viewagreement', 'AuthorController@viewAgreement')->name('view-agreement');
    Route::post('uploadagreement', 'AuthorController@uploadAgreement')->name('upload-agreement');
    Route::post('uploadTinyMceImage', 'UserAssignmentSubmissionController@uploadTinyMceImage');
    Route::post('uploadassignmentvideo', 'UserAssignmentSubmissionController@uploadAssignmentVideo');
    
    Route::post('uploadgstundertaking', 'AuthorController@uploadGstUndertaking')->name('upload-gstundertaking');
    Route::get('viewgstundertaking', 'AuthorController@viewGstUndertaking')->name('view-gstundertaking');
    Route::post('uploadinvoice', 'InvoiceController@uploadinvoice')->name('upload-invoice');
    Route::get('selfassignment', 'SelfAssignmentController@createSelfAssignment')->name('self-assignment');
    Route::post('updateselfassignment', 'SelfAssignmentController@updateSelfAssignment')->name('update-self-assignment');
    Route::get('userselfsubcategory', 'SelfAssignmentController@getUserSelfSubcategoryList')->name('user-self-subcategory');
    
});

/**
 * Admin, editor and finance user routes
 */
Route::group(array('domain' => env('ADMIN_DOMAIN')), function() {
    Route::get('lms/login', 'CustomLoginController@loginForm')->name('lms-login');
    Route::post('lms/login', 'CustomLoginController@login');
    Route::get('lms/logout', 'CustomLoginController@logout');
    Route::post('lms/logout', 'CustomLoginController@logout')->name('lms-logout');
    
    /** Affilate Dashboard Routes start ***/
    Route::get('lms/campaign/create', 'CampaignController@create')->name('campaign-create');
    Route::get('lms/campaign/list', 'CampaignController@listCampaign')->name('campaign-list');
   // Route::get('lms/campaign/create', 'CampaignController@save')->name('campaign-save');
    Route::get('lms/author/get-users-by-affiliate-type', 'CampaignController@getUsersByAffiliateType')->name('get-users-by-affiliate-type');
    
    /** Affilate Dashboard Routes end ***/
    
    Route::post('api/article/publishcallback', 'UserController@articlePublishCallback')->name('article-publishcallback');
    
    if (env('APP_ENV') != 'production') {
        Route::post('/gen-invoice', 'AssignmentController@genInvoice')->name('genInvoice');
        Route::get('/enable-payment-details', 'AssignmentController@enablePaymentDetails')->name('enablePaymentDetails');
    }
    
    
    Route::middleware('auth', 'role:editor|admin|sub-editor|reviewer')->group(function () {
        // Route::get('lms/login','Author@lmsLogin')->name('lms.login');

        Route::get('lms/author/list', 'AuthorController@index')->name('author.list');
        Route::get('lms/author/update/{id?}', 'AuthorController@authorProfileForm')->name('author.edit');
        Route::post('lms/author/save-author', 'AuthorController@updateAuthor')->name('author.save');

        Route::get('lms/assignments/list', 'AssignmentController@index')->name('assignment-list');

        Route::get('lms/assignments/create', 'AssignmentController@create')->name('assignment.create');
        Route::get('lms/assignments/create/{id?}', 'AssignmentController@create')->name('assignment.edit');
        Route::get('lms/assignments/view/{id}', 'AssignmentController@view')->name('assignment.view');
        Route::post('lms/assignments/save-assignment', 'AssignmentController@upsertAssignment')->name('save-assignment');
        Route::get('lms/assignments/review-list', 'UserAssignmentSubmissionController@reviewList')->name('assignment-review-list');
        Route::get('lms/assignments/review/{id}', 'UserAssignmentSubmissionController@review')->name('assignment-review');
        Route::post('lms/assignments/review-re-post', 'UserAssignmentSubmissionController@reviewrepost')->name('assignment-review-re-post');

        Route::get('lms/actionUpdateStatus', 'UserController@actionUpdateStatus')->name('update.status');
        Route::get('lms/author/view/{id}', 'AuthorController@actionView')->name('author.view');
        Route::get('lms/author/profile/{id}', 'AuthorController@authorProfileView')->name('author.profile');
        Route::post('lms/author/reviewpaymentdetail', 'AuthorController@actionReviewPaymentDetail')->name('review.paymentdetail');
        Route::post('lms/author/downloadfile','AuthorController@actionDownloadFiles')->name('download-file');
        Route::get('lms/author/get-users-by-assignment-type-and-language', 'AssignmentController@getUsersByAssignmentTypeAndLanguage')->name('get-users-by-assignment-type-and-language');

        Route::get('lms/assignment/view-logs/{id}', 'AssignmentController@viewAssignmentLogById')->name('view-assignment-logs');
        Route::post('lms/assignment/pushtodenmark', 'UserAssignmentSubmissionController@pushToDenmark')->name('push-denmark');
        Route::post('lms/author/redo', 'UserDetailReviewController@redoUserDetail')->name('redo-userdetail');
        
    });

    Route::middleware('auth', 'role:admin|editor')->group(function () {
        Route::get('lms/actionUpdateStatus', 'UserController@actionUpdateStatus')->name('update.status');
        Route::get('lms/create-lms-user', 'CustomLoginController@addLMSUsersForm')->name('create-lms-user-form');
        Route::get('lms/create-lms-user/{id}', 'CustomLoginController@addLMSUsersForm')->name('edit-lms-user-form');
        Route::post('lms/create-lms-user', 'CustomLoginController@registerUser')->name('register-lms-user');
        Route::post('lms/edit-lms-user', 'CustomLoginController@editLMSUser')->name('edit-lms-user');
        Route::get('lms/lms-user-list', 'CustomLoginController@lmsUserList')->name('lms-user-list');
        Route::get('lms/roles-manager', ['uses' => 'RoleController@index'])->name('roles-manager');
        Route::get('lms/roles-edit/{id}', ['uses' => 'RoleController@edit'])->name('roles-edit');
        Route::get('lms/roles-create', ['uses' => 'RoleController@create'])->name('roles-create');
        Route::post('lms/roles-save', ['uses' => 'RoleController@save'])->name('roles-save');
        Route::get('lms/roles-delete/{id}', ['uses' => 'RoleController@delete'])->name('roles-delete');

        Route::get('lms/permission-manager', ['uses' => 'PermissionController@index'])->name('permission-manager');
        Route::get('lms/permission-create', ['uses' => 'PermissionController@create'])->name('permission-create');
        Route::post('lms/permission-save', ['uses' => 'PermissionController@save'])->name('permission-save');
        Route::get('lms/permission-edit/{id}', ['uses' => 'PermissionController@edit'])->name('permission-edit');
        Route::get('lms/permission-delete/{id}', ['uses' => 'PermissionController@delete'])->name('permission-delete');

        Route::get('lms/payment/view-invoice-detail/{id}', 'InvoiceController@viewInvoiceDetail')->name('view.invoice.detail');
        
        
        Route::post('lms/upload/uploadimage', 'UserController@uploadImage')->name('upload-image');
        Route::get('lms/upload/imageform', 'UserController@uploadImageForm')->name('upload-imageform');
    });

    Route::middleware('auth', 'role:finance|admin')->group(function () {
        Route::post('lms/payments/pay-now', 'UserAssignmentSubmissionController@payNowAction')->name('pay-now-action');

        Route::get('lms/payments/invoice-list', 'InvoiceController@listInvoice')->name('invoice-list');
        Route::get('lms/payments/pay-user/{user_id}', 'UserAssignmentSubmissionController@payUser')->name('payment-pay-user');
        Route::get('lms/payments/users-account-list', 'AuthorController@usersAccountList')->name('users-account-list');
        Route::post('lms/payment/enablediablepayment/{user_id}', 'AuthorController@actionEnableDisablePayment')->name('review.enablediablepayment');
        Route::get('lms/invoice/to-excel', 'InvoiceController@exportToExcel')->name('invoice-export-to-excel');
        Route::post('lms/invoice/from-excel', 'InvoiceController@importFromExcel')->name('invoice-import-from-excel');
        Route::post('lms/invoice/exportusersaccountlist', 'InvoiceController@exportUsersAccountList')->name('export-user-account-list');
        
        Route::get('lms/payments/current-pay-user/{user_id}', 'UserAssignmentSubmissionController@currentMonthUserPayAmount')->name('current-pay-user');
    });
    Route::get('lms/payment/download-invoice/{id}', 'UserAssignmentSubmissionController@downloadInvoice')->name('download-userinvoice');
});
