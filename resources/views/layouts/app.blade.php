<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('assets/images/favicon-32x32.png')}}" type="image/png" sizes="32x32">    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'LIPI') }}</title>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js')}}"></script>   
    <!-- Scripts -->
    <!--<script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="{{ asset('js/jquery.multiselect.js') }}" defer></script>
     <script src="{{ asset('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script>

     <!-- Bootstrap Core Js -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.js')}}"></script>
    <!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.js')}}"></script>

    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!--<link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- custom_style -->
     <link href="{{ asset('css/custom_style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/jquery.multiselect.css') }}" rel="stylesheet">
    
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
    
    <!-- Bootstrap Core Css -->
    <link href="{{ asset('assets/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    
    <script>
        var tLoginObj = {
  "ssochannel":"<?php echo env('APP_CHANNEL')?>",
  "ssoUrl":"<?php echo env('SSO_URL')?>",
  "home_url":"<?php echo env('APP_URL')?>",
  "fbclient":"<?php echo env('FB_CLIENT')?>",
  "gPlusclient":"<?php echo env('GOOGLE_CLIENT')?>",
  "myTimesUrl":"https://mytest.indiatimes.com",
  "MytCommentApiKey" :"NBTB",
  "siteId" :"<?php echo env('SITE_ID')?>",
  "domainName" :"<?php echo env('DOMAIN_NAME')?>",
  "isEnvironment":"test",
  "socialappsintegrator":"<?php echo env('SOCIAL_APPS_INTEGRATOR')?>"
  
};
    </script>
    <script src="{{ asset('js/select2.min.js') }}" defer></script>
    <script src="{{ asset('js/user.custom.js')}}"></script>
    <script src="{{ asset('assets/js/pages/forms/form-validation.js')}}"></script>
     @stack('scripts')
</head>
<body class="theme-welcome">
    <div id="app">
        
            <div class="container">
                <header class="header">
			<a href="#" class="logo"><img src="/assets/images/lipi-logo.png" /></a>
		</header>
                
            </div>
        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>