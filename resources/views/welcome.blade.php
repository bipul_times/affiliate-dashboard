@php
$cookieDomainUrl = env('APP_URL');
$cookieDomainUrl = @str_replace(['http://','https://'],'',$cookieDomainUrl);
@endphp
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <title>Lipi</title>
        <link rel="icon" href="{{ asset('assets/images/favicon-32x32.png')}}" type="image/png" sizes="32x32">
        <script>
       var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
  var eventer = window[eventMethod];
  var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
  // Listen to message from child window
  eventer(messageEvent,function(e) {
    console.log('origin: ', e.origin)
	  
    // Check if origin is proper
    if( e.origin != '<?php echo env('APP_URL')?>' ){ return }
    console.log('parent received message!: ', e.data.data);
    if(e.data.data){
    __sso(e.data.data);
    }
  }, false);
  
    var tLoginObj = {
  "ssochannel":"<?php echo env('APP_CHANNEL')?>",
  "ssoUrl":"<?php echo env('SSO_URL')?>",
  "home_url":"<?php echo env('APP_URL')?>",
  "fbclient":"<?php echo env('FB_CLIENT')?>",
  "gPlusclient":"<?php echo env('GOOGLE_CLIENT')?>",
  "myTimesUrl":"https://mytest.indiatimes.com",
  "MytCommentApiKey" :"NBTB",
  "siteId" :"<?php echo env('SITE_ID')?>",
  "domainName" :"<?php echo env('DOMAIN_NAME')?>",
  "isEnvironment":"test",
  "socialappsintegrator":"<?php echo env('SOCIAL_APPS_INTEGRATOR')?>"
  
};





    </script>
     
      <script type='text/javascript' src="{{ asset('assets/plugins/jquery/jquery.min.js')}}"></script> 
      <script type='text/javascript' src='<?php echo env('JSSO_CDN')?>/crosswalk/jsso_crosswalk_0.2.5.min.js?ver=1.0'></script>
      <script type='text/javascript' src="{{ asset('js/sso/sso.js') }}" defer></script>
      
      <script src="https://www.google.com/recaptcha/api.js?ver=10.065" ></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
        
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
        <link href="{{ asset('css/custom_style.css') }}" rel="stylesheet">

    </head>
    <body class="theme-welcome">
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
	<div class="container-lipi">
		<header class="header">
			<a href="#" class="logo"><img src="{{ asset('assets/images/lipi-logo.png')}}" /></a>
		</header>
		<section class="inner-content">
			<h2>
				"<b>Lipi</b> - a platform to write and earn... designed specially for all freelance writers / translators"
			</h2>
			<h3>
				Lipi is designed specifically for those freelance writers who are looking for online content writing or content translation assignments in various Indian languages and earn according to their contribution.
			</h3>
                    <div class="btn-block">
                        <a data-plugin="user-notloggedin" class="btn active"> 
                            <span data-plugin="user-login" style="cursor:pointer">Sign In </span>
                        </a>
                        <a data-plugin="user-notloggedin" class="btn"> 
                            <span data-plugin="user-login" style="cursor:pointer">Register </span>
                        </a>
                    </div>
		</section>
	</div>
        <div style="" class="" id="login-popup">
                    <button class="close-btn" type="button">+</button>
                    <div id="signin_box">                        
                    </div>
                </div>
 </body>

</html>
<script defer>    
    setTimeout(function(){
    getCookieSsoId();
}, 200);</script>

