@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="gender" required="required">
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="mobile" class="col-md-4 col-form-label text-md-right">{{ __('Mobile') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="mobile" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile">

                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                        

                        
                        <div class="form-group row">
                            <label for="country" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}</label>

                            <div class="col-md-6">
                                <select id="country" name="country" class="form-control" style="width:350px" >
                                    <option value="" selected disabled>Select</option>
                                    @foreach($countries as $key => $country)
                  <option value="{{$key}}"> {{$country}}</option>
                  @endforeach

                                </select>
                            </div>
                            </div>
                        
                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('State') }}</label>

                            <div class="col-md-6">
                                <select name="state" id="state" class="form-control" style="width:350px">
                                    <option value="" selected disabled>Select</option>
                                </select>
                            </div>
                            </div>
                        
                        <div class="form-group row">
                            <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('City') }}</label>

                            <div class="col-md-6">
                                <select name="city" id="city" class="form-control" style="width:350px">
                                        <option value="" selected disabled>Select</option>
                            </select>
                            </div>
                            </div>
                        
                        
            
         <div class="form-group row">
                            <label for="content_writer_lang" class="col-md-4 col-form-label text-md-right">{{ __('Content Writing') }}</label>

                            <div class="col-md-6">
                                <select multiple id="content_writer_lang_id" name="content_writer_lang_id[]" class="form-control" style="width:350px" >
                                    <!--<option value="" selected disabled>Select</option>-->
                                    @foreach($languages as $key => $language)
                                    <option value="{{$key}}"> {{$language}}</option>
                                    @endforeach

                                </select>
                            </div>
                            </div>
                        
                        <div class="form-group row">
                            <label for="content_trans_from" class="col-md-4 col-form-label text-md-right">{{ __('Content Translation From') }}</label>

                            <div class="col-md-6">
                                <select multiple id="content_trans_from_id" name="content_trans_from_id[]" class="form-control" style="width:350px" >
                                    <!--<option value="" selected disabled>Select</option>-->
                                    @foreach($languages as $key => $language)
                                    <option value="{{$key}}"> {{$language}}</option>
                                    @endforeach

                                </select>
                            </div>
                            </div>
                        
                        <div class="form-group row">
                            <label for="content_trans_to" class="col-md-4 col-form-label text-md-right">{{ __('Content Translation To') }}</label>

                            <div class="col-md-6">

                                <select multiple id="content_trans_to_id" name="content_trans_to_id[]" class="form-control" style="width:350px" >
                                    <!--<option value="" selected disabled>Select</option>-->
                                    @foreach($languages as $key => $language)
                                    <option value="{{$key}}"> {{$language}}</option>
                                    @endforeach

                                </select>

                            </div>
                            </div>
            
                        
                        <div class="form-group row">
                            <label for="brief" class="col-md-4 col-form-label text-md-right">{{ __('Brief') }}</label>

                            <div class="col-md-6">
                                <!--<input id="brief" type="brief" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile">-->

                               <textarea rows="5" name="brief" class="form-control"></textarea>
                            </div>
                            </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        
        $('#content_trans_to_id').multiselect({
            columns: 1,
            placeholder: 'Select Languages'
        });
        $('#content_trans_from_id').multiselect({
            columns: 1,
            placeholder: 'Select Languages'
        });
        $('#content_writer_lang_id').multiselect({
            columns: 1,
            placeholder: 'Select Languages'
        });
    $('#country').change(function(){
    var countryID = $(this).val(); 
    //alert('HH');
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });
   });
</script>
@endsection

