<?php 
//print_r($requestData);die;
$fbCode = $requestData['code'];
$state = $requestData['state'];

$stateData = json_decode(stripslashes($state), true);
$oauthsiteid = $stateData['oauthsiteid'];
$env = '';
?>

<!DOCTYPE HTML>
<html>
   <head>
      <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>SSO</title>
      <script type="text/javascript">
         
        // cross domain checks
         var hdomain = 'indiatimes.com';
         if (document.domain != hdomain)
         {
             if ((document.domain.indexOf(hdomain)) != -1)
                {
                    document.domain = hdomain
                }
         }  
         //document.domain = 'indiatimes.com';
         var oauthsiteid = '<?php echo $oauthsiteid;?>';
      </script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
   </head>
   <body>
      Loading.....
      <script type="text/javascript" src="<?php echo env('JSSO_CDN')?>/crosswalk/jsso_crosswalk_0.2.5.min.js"></script>
      <script type="text/javascript">
         var CHANNEL = '<?php echo env('APP_CHANNEL')?>';         
         var SSO_BASE_URL = '<?php echo env('SSO_URL')?>';
         var SOCIAL_APP_URL = '<?php echo env('SOCIAL_APPS_INTEGRATOR')?>/socialsite';
         var INDEX_PAGE = '/';
         
        
         function checkLogin(){
                console.log('check self cookie MSCSAuthDetails') ;
                var logindtls = getCookie("MSCSAuthDetails");
                if(logindtls == null || logindtls == undefined) {
                    //loadScript();
                    loadScript(SSO_BASE_URL + '/sso/crossdomain/getTicket?callback=handleTicket&version=v1');
                } else {                
                    this.location.href = '/';
                }
         }
         function handleTicket(data) {
                var ticketid = data.ticketId;
                if(ticketid != null && ticketid != undefined && ticketid.length > 0) {
                        loadScript(SOCIAL_APP_URL + '/v1validateTicket?ticketId='+ ticketid + '&channel=' + CHANNEL+'&callback=handleTicketValidation');
                } else {
                        // user is not logged in, do nothing.
                        this.location.href = '/';
                }
         }
         function loadScript(url) {
             
                var script = document.createElement('script');
                script.async=true;
                script.type = 'text/javascript';
                script.src = url;
                document.body.appendChild(script);   
         }
         function handleTicketValidation(data){
                this.location.href = '/';
         }
         function __loginsuccess(){
             setTimeout(function(){
              if(isPopupActive()){
               setTimeout(function(){document.body.innerHTML = 'Please close this window if it does not close automatically.'}, 5000);
              }else{
                 document.body.innerHTML = 'Login Successful!<br/>Please close this window or go to <a href="http://timesofindia.indiatimes.com/" target="_blank" style=" color: blue; text-decoration: underline; ">Times of India</a>'
              }
             }, 5000);
         }
         
         
         function getCookie(c_name) {
             var i,x,y;
                var cookieStr = document.cookie.replace("UserName=", "UserName&");
                var ARRcookies=cookieStr.split(";");
         
             for (i=0;i<ARRcookies.length;i++)
             {
                 x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
                 y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
                 x=x.replace(/^\s+|\s+$/g,"");
                 if (x==c_name)
                 {
                     return unescape(y);
                 }
              }
         }
         
         function isPopupActive(){
            if (window.opener){
                 return !!opener;
            }
            else{
                 return window.parent.location.href != window.location.href;
            }
         }
         
         
         
         (function(){   
        
            var jssoObj = {};
            if (typeof JssoCrosswalk === 'function') {
                jssoCrosswalkObj = new JssoCrosswalk(CHANNEL, 'web');
                if(oauthsiteid == 'facebook'){
                    var redirectURL = '<?php echo env('APP_URL')?>/t-login-sso';
                    jssoCrosswalkObj.facebookLogin('<?php echo $fbCode ?>', redirectURL, fbLoginCallback);
                }else if(oauthsiteid == 'googleplus'){
                    var redirectURL = '<?php echo env('APP_URL')?>/t-login-sso';
                    jssoCrosswalkObj.googleplusLogin('<?php echo $fbCode ?>', redirectURL, fbLoginCallback);
                }
            }
            
            function fbLoginCallback(data){
                console.log('ddddd');
                console.log(data);
                
                var where = '';
                try{
                    if(!data.site){
                        data.site = oauthsiteid;
                    }
                    where = 1;
                    if(navigator.userAgent.match('CriOS')) {  // if ios chrome
                        localStorage.setItem('_ssodata',JSON.stringify(data)); 
                    }
                    where = 2;
                    window.opener.postMessage({data}, '<?php echo env('APP_URL')?>/');
//                    if(window.opener && window.opener.__sso){
//                        window.opener.__sso(data);
//                    }
                    where = 3;
                    __loginsuccess();
                    where = 4;
                    console.log(where,'where');
                    return false;
                    if(isPopupActive()){
                        where = 5;
                         this.close();
                    }else{
                          if (data.ru && data.ru.length > 0 ){
                             this.location.href = encodeURIComponent(data.ru);
                         }
                         else{
                             this.location.href = '/';
                         }
                    }
                }catch(err){
                    console.log('here = '+where);
                    console.log(err);
                    //return false;
                    checkLogin();
                }
            }
         }()); 
      </script>
   </body>
</html>
