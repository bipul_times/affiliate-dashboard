<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendEmailJob;
use App\models\Language;
use App\models\UserPaymentDetail;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','brief','location','gender','content_writer_lang','content_trans_from','content_trans_to','state_id','country_id','role_id','sso_id','category_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function setAttribute($key, $value) {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
    }
    
    static  function getLmsUserByEmail($email){
        $user = self::where('is_cms_user','1')
                ->where('email', $email)
                ->where('status', 'approved')
                ->where('is_verified', '1')->first();
        return $user;
    }
    
    public function getUserDataByUserId($id){
        $user = $this->where('id', $id)->first();
        return $user;
    }
    static function getAuthors($condition) {
       // Enable query log       
         
        return $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'author');            
        })->where($condition)
                ->leftJoin('countries', 'countries.id', '=', 'users.country_id')
                ->leftJoin('states', 'states.id', '=', 'users.state_id')
                ->leftJoin('cities', 'cities.id', '=', 'users.location')
                //->leftJoin('category', 'category.id', '=', 'users.category_id')
                ->select('users.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
                ->orderBy('users.name', 'ASC')->get();
 
         // Show results of log
        

    }
    
    
    static function getVerifiedUserById($condition) {
       // Enable query log       
         
        return $users = self::where($condition)
                ->leftJoin('countries', 'countries.id', '=', 'users.country_id')
                ->leftJoin('states', 'states.id', '=', 'users.state_id')
                ->leftJoin('cities', 'cities.id', '=', 'users.location')
                //->leftJoin('category', 'category.id', '=', 'users.category_id')
                ->select('users.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
                ->orderBy('users.name', 'ASC')->first();
 
         // Show results of log
        

    }
    
    static function getPengingAuthors($condition,$filters=[]) {
         
         $users = self::where($condition)->where('is_cms_user','0');
                if ($filters) {
                   if (isset($filters['st']) && !empty($filters['st'])) {
                       $users = $users->whereIn('users.status', [$filters['st']]);
                   } else {
                       $users = $users->whereIn('users.status', ['pending', 'approved']);
                   }
                   if (isset($filters['fd']) && !empty($filters['fd'])) {
                       $users = $users->where('users.approved_date', '>=', $filters['fd']);
                   }
                   if (isset($filters['td']) && !empty($filters['td'])) {
                       $users = $users->whereDate('users.approved_date', '<=', $filters['td']);
                   }
                   

                   if (isset($filters['q']) && !empty($filters['q'])) {
                       $users = $users->Where('users.name', $filters['q']);
                       $users = $users->OrWhere('users.email', $filters['q']);
                       $users = $users->OrWhere('users.mobile', $filters['q']);
                   }
               }


               $users = $users->where('users.id', '!=',Auth::user()->id)
                 ->leftJoin('user_price_type as upt', function($join) {
                    $join->on('upt.user_id', '=', 'users.id');
                    $join->where('upt.status', '=', 1);
                })
                ->leftJoin('countries', 'countries.id', '=', 'users.country_id')
                ->leftJoin('states', 'states.id', '=', 'users.state_id')
                ->leftJoin('cities', 'cities.id', '=', 'users.location')
                ->select('users.*','countries.name as country_name','states.name as state_name','cities.name as city_name','upt.writer_type','upt.price_type','upt.is_aggrement_received','upt.self_attest_aggrement')
                ->orderBy('users.updated_at', 'DESC')->paginate(Config::get('toi.perPage'));
                return $users;
        
        
 
    }
    
    
    static function getLmsUsersList($condition = [],$loginUser=[]){
       
        /*return $users =  User::where($condition)
                    ->rightJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.id','users.name', 'users.email','users.status', 'roles.name as role_name')
                ->orderBy('users.created_at', 'DESC')->paginate(Config::get('toi.perPage'));
         * 
         */
        DB::enableQueryLog();
         $users = self::select('users.id','users.name', 'users.email','users.status');
                    $users = $users->where('is_cms_user', '1');               
                    $users = $users->where(function ($query)use ($loginUser)  {                    
                                if($loginUser->content_writer_lang) {
                                    $query->whereIn("users.content_writer_lang", array_map('intval',explode(',', $loginUser->content_writer_lang)));
                                }
                                if($loginUser->content_trans_from) {
                                    $query->orwhereIn("users.content_trans_from", array_map('intval',explode(',', $loginUser->content_trans_from)));
                                }
                                if($loginUser->content_trans_to) {
                                    $query->whereIn("users.content_trans_to", array_map('intval',explode(',', $loginUser->content_trans_to)));
                                }
                                if ($loginUser->category_id) {
                                    $query->whereIn("users.category_id", array_map('intval', explode(',', $loginUser->category_id)));
                                }

                            });
                    

                    $users = $users->orderBy('users.created_at', 'DESC')->paginate(Config::get('toi.perPage'));
                    //dd(DB::getQueryLog());
                    //echo '<pre>';print_r($users->toArray());die;
                    return $users;
     
    }
    
    static function getUserById($id){
        
       
        return $users =  User::where('users.id', $id)->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
                ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
                ->select('users.*','roles.name as role_name')
                ->orderBy('users.created_at', 'DESC')->firstOrFail();
        
        
    }
    

    static function getEligibleUsersByLanguage($assignment_type, $language_id=0, $from_lang=0, $to_lang=0, $userIdsToInvite='', $sub_category_id) {
            //print_r($userIdsToInvite);die;
       
        $query = User::select('email', 'name');
        if($assignment_type == 'CW')
            $query =  $query->whereRaw("FIND_IN_SET(?, users.content_writer_lang)", array($language_id));
        else{
            $query =  $query->whereRaw("FIND_IN_SET(?, users.content_trans_from)", array($from_lang));
            $query =  $query->whereRaw("FIND_IN_SET(?, users.content_trans_to)", array($to_lang));
        }
        if($userIdsToInvite != ''){
            $query =  $query->whereIn('users.id', $userIdsToInvite);
        }
        //if($sub_category_id){
            $query =  $query->whereRaw("FIND_IN_SET(?, users.sub_category_id)", array($sub_category_id));
       // }
        $query =  $query->where('users.is_cms_user', '0');
        $query =  $query->get();
       
        $res = [];
        foreach ($query as $key => $val){
            $res[$key]['email'] = $val->email;
            $res[$key]['name'] = $val->name;
        }
        return $res;
    }
    
    static function getEligibleUsersByAssignmentTypeAndLanguage($assignment_type, $language_id=0, $from_lang=0, $to_lang=0, $assignment_id=null, $sub_category_id=null) {
        $query = User::select('id','email', 'name', 'mobile');
        if($assignment_type == 'CW')
            $query =  $query->whereRaw("FIND_IN_SET(?, users.content_writer_lang)", array($language_id));
        else{
            $query =  $query->whereRaw("FIND_IN_SET(?, users.content_trans_from)", array($from_lang));
            $query =  $query->whereRaw("FIND_IN_SET(?, users.content_trans_to)", array($to_lang));
        }        
        $query =  $query->whereRaw("FIND_IN_SET(?, users.sub_category_id)", array($sub_category_id));        
        $query =  $query->where('users.is_cms_user', "0");
        $query =  $query->get();
        //dd(DB::getQueryLog());
        $invalidUser = [];
        $invalidUser = models\UserAssignmentSubmission::getUserAssignmentExpiredListSubmissionByUserIDANDAssignmentID( $assignment_id);
        $invalidUser = $invalidUser->toArray();
        $filteredUsers = [];
        foreach($invalidUser as $i => $invalid){
            $filteredUsers[] = $invalid['user_id'];
        }
        $res = [];
        foreach ($query as $key => $val){
            if(!in_array($val->id, $filteredUsers)){                
                $res[$key]['user_id'] = $val->id;
                $res[$key]['mobile'] = $val->mobile;
                $res[$key]['email'] = $val->email;
                $res[$key]['name'] = $val->name;
            }            
        }
        return $res;
    }
    
    public static function getUserByEmail($email){
        return  self::where('email',$email)->first();
    }

    public static function getUserByssoId($ssoId){
        return  self::where('sso_id',$ssoId)->first();
    }
    public static function getUserByMobile($mobileNo){
        return  self::where('mobile',$mobileNo)->first();
    }
    
    static function getUsersByRoleId($roleId){
         return DB::table('role_user')->where('role_id', $roleId)->pluck('user_id');        
    }
    
    static function mailUserSubmittedDoc($user_id) {
        $user = User::getUserById($user_id);
        $assignmentParams = [];
        $assignmentParams['mailSubject'] = 'Documentation Process Completed For - '.$user->name;
        $assignmentParams['mailMethod'] = 'user_doc_submitted';
        $assignmentParams['bladeViewName'] = 'mail.user_doc_submitted';
        $assignmentParams['authorName'] = $user->name;
        $assignmentParams['authorEmail'] = $user->email;
        
        $editorToMailId = Config::get('toi.EditorsLanguageMailId')['Default_editor'];
        $editorData = [];
        $editorData[0]['name'] = 'Editor';
        $editorData[0]['email'] = $editorToMailId;
        return dispatch(new SendEmailJob($assignmentParams, $editorData));
    }
    
    static function mailPaymentDetailsApproved($userDetail, $priceDetail){
        $userPrefCombination = self::getPreferenceCombination($userDetail);
        
        $price_type = $misc_pay_type='';
       
        $assignmentParams = [];
        $assignmentParams['CT'] = $userPrefCombination['CT'];
        $assignmentParams['CW'] = $userPrefCombination['CW'];
        $userRole[] = $assignmentParams['CW'];
        $userRole[] = $assignmentParams['CT'];
        $userRoleFinal = @implode('/', $userRole);
        $assignmentParams['user_role'] = $userRole;
        $assignmentParams['writer_type'] = ucwords($priceDetail->writer_type);        
        if($priceDetail->writer_type == 'freelancer'){
            if($priceDetail->price_type == 'PA'){
                $price_type = 'per article';
                $assignmentParams['payment_type_in_details'] = ' INR '
                    .number_format((float) $priceDetail->amount, 2, '.', '')
                    .' '.$price_type;  
            }else if($priceDetail->price_type == 'PW'){
                $price_type = 'per word';
                $assignmentParams['payment_type_in_details'] = ' INR '
                    .number_format((float) $priceDetail->amount, 2, '.', '')
                    .'p '.$price_type;
            }else if($priceDetail->price_type == 'Miscellaneous'){
                $price_type = 'per article';
                $misc_pay_type = "Miscellaneous";
                $assignmentParams['payment_type_in_details'] = ' INR ';
            }
            
        }else if($priceDetail->writer_type == 'intern' || $priceDetail->writer_type == 'stringer'){
            $price_type = $priceDetail->price_type;            
            $assignmentParams['payment_type_in_details'] = ' INR '
                    .number_format((float) $priceDetail->amount, 2, '.', '')
                    .' Per '.$priceDetail->max_article.' articles';  
        }
       
        $assignmentParams['isgstundertaking'] = false;
        if(UserPaymentDetail::isGstUndertaking($userDetail->id)){
            $assignmentParams['isgstundertaking'] = true;
        }
        $assignmentParams['price_type'] = $price_type; 
        $assignmentParams['misc_pay_type'] = $misc_pay_type;
        $assignmentParams['amount'] = $priceDetail->amount;
        
        $assignmentParams['mailSubject'] = 'Approved for the Role of '.$userRoleFinal;
        $assignmentParams['mailMethod'] = 'user_payment_approved';
        $assignmentParams['bladeViewName'] = 'mail.user_payment_approved';
        
        $authorData = [];
        $authorData[0]['name'] = $userDetail->name;
        $authorData[0]['email'] = $userDetail->email;
        
        return dispatch(new SendEmailJob($assignmentParams, $authorData));
    }
    
    static function getPreferenceCombination($userDetail){
        $res = [];
        $content_trans_from = Language::getLanguageByIds(explode(',', $userDetail->content_trans_from));
        $content_trans_to = Language::getLanguageByIds(explode(',', $userDetail->content_trans_to));
        $combination = '';
        foreach($content_trans_from as $key => $from){
            foreach($content_trans_to as $to_key => $to){                
                $combination .= $from->short_code.'2'.$to->short_code.',';
            }
        }
        if($combination){
            $combination = 'CT-'.$combination;
        }
        $writer_lang = '';
        $content_writer_lang = Language::getLanguageByIds(explode(',', $userDetail->content_writer_lang));
        foreach($content_writer_lang as $key => $lang){
            $writer_lang .= $lang->short_code.',';
        }
        if($writer_lang){
            $writer_lang = 'CW-'.$writer_lang;
        }
        $res['CT'] = rtrim($combination,',');
        $res['CW'] = rtrim($writer_lang,',');;
        
        return $res;
        
    }
    
    
    static function getAuthorsWithPaymentDetails($filters = [],$isPaginat=true) {
         // Enable query log       
         
        $users = User::select('users.id as user_id','users.name', 'users.email', 
                'users.mobile','users.status as user_status', 'users.approved_date',
                'UPT.writer_type','UPT.price_type', 'UPD.pancard_number',
                'UPD.aadhar_number','UPD.aadhar_image',
                'UPD.account_number','UPD.bank_name','UPD.branch_name','UPD.ifsc_code',
                'UPD.gst_number','UPD.gst_type',
                'UPD.name_declaration_image','UPD.pancard_image',
                'UPD.gst_undertaking','UPD.address','UPD.state_id','UPD.city_id' )
                ->leftJoin('user_payment_details as UPD', function($join) {
                    $join->on( 'UPD.user_id', '=', 'users.id');
                })
                ->leftJoin('user_price_type as UPT', function($join) {
                    $join->on('UPT.user_id', '=', 'users.id');
                    $join->where('UPT.status', '=', 1);
                })
                
                ->whereHas('roles', function ($query) {
                    $query->where('name', 'author');            
                });
                if($filters != []){            
                    if(isset($filters['frm_date']) && !empty($filters['frm_date'])){
                        $users = $users->where('users.approved_date', '>=', $filters['frm_date']);
                    }
                    if(isset($filters['to_date']) && !empty($filters['to_date'])){
                        $users = $users->whereDate('users.approved_date', '<=', $filters['to_date']);
                    }
                    if(isset($filters['writer_type']) && !empty($filters['writer_type'])){
                        $users = $users->where('UPT.writer_type', $filters['writer_type']);
                    }
                    
                    if(isset($filters['q']) && !empty($filters['q'])){
                        $users = $users->Where('UPD.pancard_number', $filters['q']);            
                        $users = $users->OrWhere('users.name', $filters['q']);
                        $users = $users->OrWhere('users.email', $filters['q']);
                        $users = $users->OrWhere('users.mobile', $filters['q']);
                    }
                }
                $users= $users->where('users.is_verified' , '1')
                ->where('users.is_cms_user' , '0')
                ->whereIn('users.status', ['approved','blocked']);
                        
                $users= $users->orderBy('users.approved_date', 'Desc');
                if($isPaginat){
                    $users= $users->paginate(Config::get('toi.perPage'));
                }else{
                    $users= $users->get();
                }
                 
                return $users;
         // Show results of log
        
    }
    
    static function mailCronLog($mailParams){
        $developerEmail[0]['email'] = Config::get('toi.devEmailLogs');
        $developerEmail[0]['name'] = 'Developer';
        
        $mailParams['mailMethod'] = 'developer_mail_alert';
        $mailParams['bladeViewName'] = 'mail.developer_mail_alert';
        $subject = 'Cron mail';
        if(isset($mailParams['mailSubject']) && !empty($mailParams['mailSubject'])){
            $subject = $mailParams['mailSubject'];
        }
        $mailParams['mailSubject'] = $subject;// 'Cron mail';
        
            
        return dispatch(new SendEmailJob($mailParams, $developerEmail));
    }
    
    /**
     * This Method Prepare Data and call Method editorNotifyAfterDocumentUplod for
     * Mail send to editor when writer / freelance upload their
     * Contract Agreement and Indemnity Form
     * Indemnity Form for non GST User only whose select GST undertaking option
     * @param type $documentType contain Agreement or Indemnity Form
     * @param type $user_id 
     */
    public static  function mailEditorNotifyAfterDocumentUplod($documentType,$user_id) {
        $editorData = $mailData=[];
        $authorDetails = User::getUserById($user_id);
        $mailData['mailMethod'] = 'editorNotifyAfterDocumentUplod';
        $mailData['document_type'] = $documentType;
        $mailData['authorName'] = $authorDetails->name;
        $mailData['authorEmail'] = $authorDetails->email;
        
        $editorToMailId = Config::get('toi.EditorsLanguageMailId')['Default_editor'];
        $editorData[0]['name'] = 'Editor';
        $editorData[0]['email'] = $editorToMailId;
        \Log::info(" mailEditorNotifyAfterDocumentUplod ".$documentType." :  " . json_encode($editorData, true));
        
        dispatch(new SendEmailJob($mailData, $editorData));
    }
    
    
    /**
    * Cron run every day
    * This Method Prepare Data and call Method notifyUserForCompleteDocumentProcess for
    * Send mail to the writer whose registration process is completed in our CMS
    * but not uploaded their documents like
    * PAN Number, Bank Details, Aadhar number etc.
    */
    public static function mailNotifyUserForPendingDocument($userId=false){
        $usersEmail =$mailData=[];
        //DB::enableQueryLog();
        $query = self::select('email', 'vendorname')->where(['is_cms_user' => '0', 'is_verified' => '1', 'status' => 'pending']);
        if ($userId) {  
            $query = $query->where(['id'=>$userId]);
        } 
        $query =  $query->get();
            
        
    
        //dd(DB::getQueryLog());
        if ($query) {
            foreach ($query as $key => $val) {
                $usersEmail[$key]['email'] = $val->email;
                $usersEmail[$key]['name'] = $val->vendorname;
            }
           // echo '<pre>';print_r($usersEmail);die;
            $mailData['mailMethod'] = 'notifyUserForCompleteDocumentProcess';
            \Log::info(" notifyUserForCompleteDocumentProcess  :  " . json_encode($usersEmail, true));
            dispatch(new SendEmailJob($mailData, $usersEmail));
        }
    }
    
    
    public static  function mailEditorNotifyAfterRedo($user_id,$reDoData) {
        $editorData = $mailData=$updatedField=$updateUploadedField=[];
        $authorDetails = User::getUserById($user_id);
        $mailData['mailMethod'] = 'editorNotifyAfterRedo';
        $mailData['authorName'] = $authorDetails->name;
        $mailData['authorEmail'] = $authorDetails->email;
        $mailData['fieldName'] = '';
        $mailData['uploadFieldName'] = '';
        //$updatedField = '';
        if ($reDoData) {
            foreach ($reDoData as $key => $value) {
                if($key=='pancard'){
                    $updatedField[] = "Pancard";
                    $updateUploadedField[] = "Pancard";
                }else if($key=='cheque'){
                    $updateUploadedField[] = "Cancelled Cheque";
                }else if($key=='aadhar'){
                    $updateUploadedField[] = "Aadhaar Card";
                    $updatedField[] = "Aadhaar Card";
                }else if($key=='gst_undertaking'){
                    $updatedField[] = "GST Undertaking";                    
                }else if($key=='gst_number'){
                    $updatedField[] = "GST Number";                   
                }else if($key=='sac_code_id'){
                    $updatedField[] = "SAC Code";                    
                }else if($key=='bank_name'){
                    $updatedField[] = "Bank Name";                    
                }else if($key=='branch_name'){
                    $updatedField[] = "Branch Name";                    
                }else if($key=='ifsc_code'){
                    $updatedField[] = "IFSC Code";                    
                }else{
                    $updatedField[] = ucfirst($key);
                }
                //$updatedField[]
            }
        }
        if($updatedField){
             $mailData['fieldName'] = @implode('/', $updatedField);
        }
        if($updateUploadedField){
             $mailData['uploadFieldName'] = @implode('/', $updateUploadedField);
        }
        
        
        $editorToMailId = Config::get('toi.EditorsLanguageMailId')['Default_editor'];
        $editorData[0]['name'] = 'Editor';
        $editorData[0]['email'] = $editorToMailId;
        \Log::info(" mailEditorNotifyAfterRedo :  " . json_encode($editorData, true));
        
        dispatch(new SendEmailJob($mailData, $editorData));
    }
    
    
    
    public static  function mailUserNotifyForRedo($user_id,$reDoData) {
        $editorData = $mailData=$updatedField=$updateUploadedField=[];
        $contractDependentContent =$editorComment='';
        $authorDetails = User::getUserById($user_id);
        $isGstUndertaking = UserPaymentDetail::isGstUndertakingExistByUserId($user_id);
        $mailData['mailMethod'] = 'userNotifyForRedo';
        $mailData['authorName'] = $authorDetails->name;
        $mailData['authorEmail'] = $authorDetails->email;
        $mailData['fieldName'] = '';
        
        $contractAgreement = "<br>Revised payment will be released only after the revised copy of the self-attested contract is uploaded and hard copy of the revised contract will be received by Times Internet at the following address.";
        $contractAgreement .="<br /><br />Address: <br />";
        $contractAgreement .="Priyanka Tyagi,<br />Times Internet Limited,<br />Hyatt Chambers,<br />";
        $contractAgreement .="1/2 Primrose Road,<br />Bangalore - 560025<br /><br />";
        $contractAgreement .="<br /><a target='_blank' href='" . env('APP_FRONT_URL') . "/view'>To Download and Upload agreement form - Click here </a>";
        $contractAgreement .="<br />or after login goto My Account -> My Info<br /><br />";
        
        $gstUnderTaking  = "Also, download indemnity form which is duly signed along with the name and signature of two witnesses has to be uploaded.";
        $gstUnderTaking .="<br /><a target='_blank' href='" . env('APP_FRONT_URL') . "/view'>To Download and Upload the indemnity form - Click here </a>";
        $gstUnderTaking .="<br />or after login goto My Account -> My Info<br /><br />";
        if ($reDoData) {
            foreach ($reDoData as $key => $value) {
                $editorComment = $value;
                if($key=='pancard'){
                    
                    $updatedField[] = "Pancard";
                    $updateUploadedField[] = "Pancard";
                    $contractDependentContent = $contractAgreement;
                    if($isGstUndertaking){
                        $contractDependentContent = $contractAgreement.$gstUnderTaking;
                        
                    }
                   
                }else if($key=='cheque'){
                    $updatedField[] = "Cancelled Cheque / Bank Manager Copy";
                    $updateUploadedField[] = "Cancelled Cheque / Bank Manager Copy";
                }else if($key=='aadhar'){
                    $updatedField[] = "Aadhaar Card";
                    $updateUploadedField[] = "Aadhaar Card";
                    $contractDependentContent = $contractAgreement;
                }else if($key=='name'){
                    $updatedField[] = ucfirst($key);
                    $contractDependentContent = $contractAgreement;
                    if($isGstUndertaking){
                        $contractDependentContent = $contractAgreement.$gstUnderTaking;
                    }
                }else if($key=='gst_undertaking'){
                    $updatedField[] = "GST Undertaking";
                    $contractDependentContent = $contractAgreement;
                }else if($key=='gst_number'){
                    $updatedField[] = "GST Number";                   
                }else if($key=='sac_code_id'){
                    $updatedField[] = "SAC Code";                    
                }else if($key=='bank_name'){
                    $updatedField[] = "Bank Name";                    
                }else if($key=='branch_name'){
                    $updatedField[] = "Branch Name";                    
                }else if($key=='ifsc_code'){
                    $updatedField[] = "IFSC Code";                    
                }else{
                    $updatedField[] = ucfirst($key);
                }
                //$updatedField[]
            }
        }
        /*indemnity form: Name, Pancard
            Agreement: Name,Pan,GST number,Aadhar number
         */
        $mailData['contractDependentContent'] = $contractDependentContent;
        if($updatedField){
             $mailData['fieldName'] = @implode('/', $updatedField);
        }
        
        if($updatedField){
             $mailData['uploadFieldName'] = @implode('/', $updateUploadedField);
        }
         $mailData['editorComment'] = $editorComment;
        
        
        
        $editorToMailId = Config::get('toi.EditorsLanguageMailId')['Default_editor'];
        $editorData[0]['name'] = $authorDetails->name;
        $editorData[0]['email'] = $authorDetails->email;
        \Log::info(" mailUserNotifyForRedo :  " . json_encode($editorData, true));
        
        dispatch(new SendEmailJob($mailData, $editorData));
    }
    
    
    static function getAffiliateTypeUsers($affiliatetype_id) {
       // Enable query log       
         
        return $users = User::whereHas('roles', function ($query) {
            $query->where('name', 'author');            
        })->whereRaw("FIND_IN_SET(?, users.affiliatetype_id)", array($affiliatetype_id))
                
                ->leftJoin('countries', 'countries.id', '=', 'users.country_id')
                ->leftJoin('states', 'states.id', '=', 'users.state_id')
                ->leftJoin('cities', 'cities.id', '=', 'users.location')
                //->leftJoin('category', 'category.id', '=', 'users.category_id')
                ->select('users.*','countries.name as country_name','states.name as state_name','cities.name as city_name')
                ->orderBy('users.vendorname', 'ASC')->get();
 
         // Show results of log
        

    }
    
   
}
