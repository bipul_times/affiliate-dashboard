<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Traits\ActivityLogTraits;

class AppServiceProvider extends ServiceProvider
{
    
    //use ActivityLogTraits;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(config('app.env') === 'production') {
            \URL::forceScheme('https');
        }
    }
    
}
