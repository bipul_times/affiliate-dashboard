<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Role;
use App\models\Permission;
use Entrust;
use App\User;
use DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        $roles = Role::getRolesList();
        return view('roles.role_list', compact('roles'));
    }
    
    public function edit(Request $request, $id) {
        $all_permissions = Permission::getPermissionsList();        
        if($id){
            $encodeID = base64_encode($id);
            //;
            $rolesPermissionDetails = Role::select(
                    'roles.id as role_id','roles.name as role_name', 'roles.description as role_desc',
                    'permissions.id as permission_id','permissions.name as permission_name', 'permissions.description as permission_desc')->leftJoin('permission_role', function($join) {
                $join->on('roles.id', '=', 'permission_role.role_id');
            })->leftJoin('permissions', function($join) {
                $join->on('permissions.id', '=', 'permission_role.permission_id');
            })->where('roles.id', $id)->get();
               
            $roleDetails = Role::getRoleById($id);
            
           return view('roles.role_edit', compact('rolesPermissionDetails', 'all_permissions', 'roleDetails', 'encodeID'));
        }
        
    }
    
    public function create() {
        $all_permissions = Permission::getPermissionsList();        
        return view('roles.create_role', compact('all_permissions'));
    }
    
    
    public function save(Request $request) {
        $validationFields = [
            'name' => 'required',
            'display_name' => 'required',
            'description' => 'required'
        ];
        $validatedData = $request->validate($validationFields);
        $id = '';
        if(isset($request['invisible_id']) && !empty($request['invisible_id'])){
            $id = base64_decode($request['invisible_id']);
        }
        
        $role = Role::firstOrNew(array('id' => $id)); 
        
        $role->name = $request['name'];
        $role->display_name = $request['display_name'];
        $role->description = $request['description'];
        $role->updated_at = date('Y-m-d H:i:s');
        $roleDetail = $role->save();
        $role_id = $role->id;
        if(isset($request->permission) && !empty($request->permission)){
            Permission::assignPermissionsToRole($role_id, $request->permission);
        }else{ // Delete all permission attached
            Permission::assignPermissionsToRole($role_id, []);
        }
        return redirect(route('roles-manager'))->with('Roles created/updated successfully');
    }
    
    
    public function delete($roleID) {
        if(!$roleID) return false;
        if(!Entrust::can('roles-delete')){
            return redirect('/')->with('error', 'Unauthorize access');
        }
        
        $userIDs = User::getUsersByRoleId($roleID);
        foreach($userIDs as $userID){ 
            //$user = User::where('id', $userID)->firstOrFail();
            //$user->detachRole($role); // in case you want to detach role
            return back()->with('error', 'Unlink Users from this role!, then try to delete.');
        }
        
        $permissionIDs = Permission::getPermissionsByRoleId($roleID);
        $role = Role::getRoleById($roleID);        
        foreach($permissionIDs as $permissionID){            
            $role->detachPermission($permissionID); // in case you want to detach permission
        }
       
        
        $role = Role::getRoleById($roleID);
        
        $role->delete();// this throws error need to look
        
        
        return back()->with('success', 'Role deleted successfully?');
    }
    
}
