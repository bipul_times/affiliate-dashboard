<?php
namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\User;
use App\models\Role;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use App\models\City;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\models\Language;
use App\models\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Entrust;
use App\models\Affiliatetypes;





class UserController extends Controller
{
    
    public function actionUpdateStatus(Request $request){
        $response = ['status' =>false,'message','Error Occured, Please try after spme time!'];
        if ($request->status && $request->id) {
            $isUpdate = User::where('id', $request->id)->update(['status' => $request->status, 'updated_at' => date('Y-m-d H:i:s')]);
            if ($isUpdate) {
                if($request->currentStatus=='pending' && in_array($request->roleType, [4,5])){
                    //echo $request->currentStatus."----".$request->roleType;die;
                    Role::assignUserRole($request->id,$request->roleType);
                }
                $response = ['status' =>true,'message','Success','authorStatus'=> ucfirst($request->status)];
                return response()->json($response);
            }
            return response()->json($response);
        }
    }
    
    
    public function updateUser(Request $request){
        $countries = DB::table("countries")->pluck("name","id");
        $languages = DB::table("language")->pluck("name","id");
        return view('auth.register',compact('countries','languages'));
    }
    
    
    public function ssoLogin(Request $request){
         //echo '<pre>';print_r($_REQUEST);die;
         $requestData['state'] = $request->state;
         $requestData['code'] = $request->code;
         $requestData['scope'] = $request->scope;
         $requestData['authuser'] = $request->authuser;
         $requestData['hd'] = $request->hd;
         $requestData['session_state'] = $request->session_state;
         $requestData['prompt'] = $request->prompt;
        //echo '<pre>';print_r($requestData->state);
        //die;
        return view('sso', compact('requestData'));
        //echo 'HHH';die;
    }
    
    public function validateSsoTicket(Request $request){
        //$ticketId = $_GET['ticketId'];
        $ticketId = $request->ticketId;
        $oauthsiteid = '';
        $type = '';
	if(empty($ticketId )) {
		// send to error page
		$URL =  env('SSO_URL').'/sso/identity/login/socialLoginErrorHandler?channel=' . env('APP_CHANNEL') . '&code=500&oauthsiteid='. $oauthsiteid . '&type=' . $type . '&msg=invalid_parameters_to_file';
		header('Location: '. $URL);
	} else {
		$URL = env('SSO_URL').'/sso/crossdomain/v1validateTicket?channel=' .
                        env('APP_CHANNEL') . 
                        '&ticketId=' . 
                        $ticketId . 
                        '&getOAuthData=true&type=JSON'; 
		//$data = download_page($URL);
                $data = file_get_contents($URL);
		$expires=time()+86400*30;
		$domainName='.'.env('DOMAIN_NAME');
		
		if($data) {
			$jsonData = json_decode($data);
			setcookie("ssoid",$jsonData->{'userId'},$expires,'/',$domainName);
		}
		echo $request->callback . '({"msg":"success","code":200});';exit;
	}
        
    }

        public function checkUser(Request $request){
            
        if ($request->ajax()) {
            $accepted_origins = array(env('APP_URL'));
            if (isset($_SERVER['HTTP_ORIGIN'])) {
                // Same-origin requests won't set an origin. If the origin is set, it must be valid.
                if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
                    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                } else {
                    header("HTTP/1.1 403 Origin Denied");
                    return;
                }
            }
            if ($request->pl) {
                $postData = json_decode(base64_decode(urldecode($request->pl)), true);
                if ($postData['email']) {
                    $request->session()->put('email', $postData['email']);
                }
                if ($postData['ssoid']) {
                    $request->session()->put('ssoid', $postData['ssoid']);
                }
                if ($postData['fname'] || $postData['lname']) {
                    $request->session()->put('name', trim($postData['fname'] . " " . $postData['lname']));
                }
                if ($postData['mobile']) {
                    $request->session()->put('mobile', $postData['mobile']);
                }
            }
            //echo 'hh1--';print_r($postData);
            if ($postData['email'] || $postData['ssoid']) {
                $userData = [];
                //$email =key($request->email);
                $email = $postData['email'];
                // key($request->email);die;
                if ($email) {
                    $userData = User::getUserByEmail($email);
                } else if ($postData['ssoid']) {
                    $userData = User::getUserByssoId($postData['ssoid']);
                }

                //
                //echo 'hh12';print_r($userData);die;
                if ($userData && $userData->is_cms_user=='0') {
                    auth()->login($userData, true);
                    if ($userData->status == 'pending') {
                        return response()->json(['status' => true, "code" => 202, 'message' => 'Pending', 'ru' => 'PAF']); //payment.form
                    } else if ($userData->status == 'blocker') {
                        return response()->json(['status' => false, "code" => 401, 'message' => 'Unauthorized', 'ru' => 'BLK']); //dashboard
                    } else {
                        return response()->json(['status' => true, "code" => 200, 'message' => 'Success', 'ru' => 'BRD']); //dashboard
                    }
                    // return response()->json(['status'=>true,'reditectUrl'=>'/user/payment-detail']);
                }elseif ($userData && $userData->is_cms_user=='1') {
                    return response()->json(['status' => false, "code" => 404, 'message' => 'Unauthorized', 'ru' => 'BLK']); //dashboard
                } else {
                    return response()->json(['status' => true, "code" => 201, 'message' => 'Created', 'ru' => 'PRF']); //profile.form
                }
            }
        }
    }
    
    
    public function userProfileForm(Request $request){
       //echo 'HH';die;
        if(Auth::check()){
            $user = Auth::User();
             return redirect()->route('home');
        }else{
            $cookieSsoId= Cookie::get('ssoid');
            if($request->session()->has('ssoid') && !empty($request->session()->get('ssoid'))){
               $user = new User();
               if ($request->session()->has('email')) {
                    $user->email = $request->session()->get('email');
               }
               if ($request->session()->has('name')) {
                    $user->name = $request->session()->get('name');
               }
               if ($request->session()->has('mobile')) {
                    $user->mobile = $request->session()->get('mobile');
               }else{
                   //$request->session()->put('mobile','9990365588');
                   $user->mobile = $request->session()->get('mobile');
               }
               if ($request->session()->has('ssoid')) {
                    $user->sso_id = $request->session()->get('ssoid');
               }
               //echo '<pre>';print_r($user);die;
            }else{
                 return response('Unauthorized.', 401);
            }
            
        }
        $catObj = new Affiliatetypes();
        $subCategoryList = $catObj->getAllAffiliate();
        //$subCategoryList = App\Helpers\Utility::parseCategoryList($allSubcat);
        //echo '<pre>';print_r($subCategoryList);die;
        $categories = Category::getCategories();
        $countries = DB::table("countries")->pluck("name","id");
        $languages = DB::table("language")->pluck("name","id");
        return view('profile', compact('countries','languages','user','categories','subCategoryList'));
    }
    
    
    public function saveUserProfile(Request $request){
        if ($request->ajax()) {
            try {
                //echo '<pre>';print_r($_REQUEST);die;
                $sessionEmail = $sessionName=$sessionMobile=$sessionSso_id='';
                $request->flash();
                $customeErrorMessagemessages = [
                    'vendorname.required' => 'The vendor name field is required!',
                    'companyname.required' => 'The company name field is required!',
                    'email.required' => 'The email field is required!',
                    //'gender.required' => 'The gender field is required!',
                    'mobile.required' => 'The mobile field is required!',
                    'affiliatetype.required' => 'The category field is required!',
                    //'mobile.regex' => 'The mobile field should be numeric!',
                    'country.required' => 'The country field is required!',
                    'state.required' => 'The state field is required!',
                    'city.required' => 'The city field is required!',
                    'companyaddress.required' => 'The company address is required!',
                ];
                $validateFields = [
                    'vendorname' => 'required',
                    'companyname' => 'required',
                    'email' => 'required|email|unique:users',
                   // 'gender' => 'required',
                    'affiliatetype' => 'required',
                    'mobile' => 'required|unique:users|regex:/^[0-9]{10}+$/',
                    'country' => 'required',
                    'state' => 'required',
                    //'city' => 'required',
                    'companyaddress' => 'required',
                ];
                
                if ($request->session()->has('email')) {
                    $sessionEmail = $request->session()->get('email');
                    $request->request->add(['email'=>$sessionEmail]);                    
                } 
//                if ($request->session()->has('name')) {
//                    $sessionName = $request->session()->get('name');
//                    $request->request->add(['name'=>$sessionName]);
//                } 
                if ($request->session()->has('mobile')) {
                    $sessionMobile = $request->session()->get('mobile');
                    $sessionMobile = @str_replace('-', '', $sessionMobile);
                    $request->request->add(['mobile'=>$sessionMobile]);
                } 
                if ($request->session()->has('ssoid')) {
                    $sessionSso_id = $request->session()->get('ssoid');
                }
                
                if(isset($request->country) && $request->country==101){
                    $customeErrorMessagemessages['city.required'] = "Please select city field!";
                    $validateFields['city'] = 'required';
                }

                if (empty($request->content_writer_lang_id)) {
                    $customeErrorMessagemessages['content_writer_lang_id.required'] = "Please select either content writing or content translation field!";
                    $validateFields['content_writer_lang_id'] = 'required';
                }
                
                if (empty($request->subcategory) ) {
                   // $customeErrorMessagemessages['subcategory.required'] = "Please select subcategory field!";
                   // $validateFields['subcategory'] = 'required';
                }

                $validator = Validator::make($request->all(), $validateFields, $customeErrorMessagemessages);
                if ($validator->fails()) {
                    $messages = $validator->errors();
                    foreach ($messages->all() as $message) {
                        $errorMessage[] = $message;
                    }
                    
                    //return back()->with('errors',$errorMessage); 
                    $responseData = ['status' => false, 'save' => false, 'message' => $errorMessage];
                    $response = response()->json(['response' => $responseData]);
                    return $response;
                }
               
               
               // echo '<pre>';print_r($parentCat);
                $user = new User();
                $user->email = ($sessionEmail) ? $sessionEmail : trim($request->email);
                $user->vendorname = ($request->vendorname) ? $request->vendorname : trim($request->vendorname);
                $user->companyname = ($request->companyname) ? $request->companyname : trim($request->companyname);
                $user->mobile = ($sessionMobile) ? $sessionMobile :  trim($request->mobile);
                $user->companyaddress = (isset($request->companyaddress) && !empty($request->companyaddress)) ? trim($request->companyaddress) : '';
                //$user->gender = $request->gender;
                $user->affiliatetype_id = ($request->affiliatetype) ? @implode(',', $request->affiliatetype) : 0;
                //$user->sub_category_id = ($request->category) ? @implode(',', $request->category) : 0;
                $user->location = (isset($request->city) && !empty($request->city)) ? $request->city : 0;
                $user->state_id = $request->state;
                $user->country_id = $request->country;
                $user->content_writer_lang = ($request->content_writer_lang_id) ? @implode(',', $request->content_writer_lang_id) : 0;
                //$user->content_trans_from = ($request->content_trans_from_id) ? @implode(',', $request->content_trans_from_id) : 0;
                //$user->content_trans_to = ($request->content_trans_to_id) ? @implode(',', $request->content_trans_to_id) : 0;
                $user->sso_id = ($sessionSso_id) ? $sessionSso_id : $request->sso_id;
                $user->status = 'pending';
                $user->is_verified = '1';
                $user->created_at = date('Y-m-d H:i:s');
                $user->password = md5(time());
                //echo '<pre>';print_r($user);die;
                if ($user->save()) {
                    auth()->login($user, true);
                    User::mailNotifyUserForPendingDocument($user->id);
                    $responseData = ['status' => true, 'save' => true, 'message' => ['success'], 'reditectUrl' => route('payment.form')];
                    $response = response()->json(['response' => $responseData]);
                    return $response;
                } else {
                    $responseData = ['status' => false, 'save' => false, 'message' => ['Error occured please try after some time!']];
                    $response = response()->json(['response' => $responseData]);
                    return $response;
                }
            } catch (\Exception $ex) {
                \Log::info("Error Profile Page:  " . $ex->getMessage());
                $responseData = ['status' => false, 'save' => false, 'message' => ['Error occured please try after some time! error code : '.$ex->getMessage()]];
                $response = response()->json(['response' => $responseData]);
                return $response;
            }
        }
    }
    
    /**
     * Method called after article publish from Denmark
     * When the article has been published in Denmark, 
     * mail will be sent to the writer along with the Live URL.
     * @return type
     */
    public function articlePublishCallback(){
        try{
            \Log::info("\n-------------articlePublish Callback start-------------\n");
            $payLoadData = [];
            $jsonPostParams = file_get_contents("php://input");
            if (strlen($jsonPostParams) > 0) {
                $payLoadData = json_decode($jsonPostParams, true);
                if (json_last_error() === JSON_ERROR_NONE) {
                    \Log::info("articlePublish Data :  " . $jsonPostParams);
                   if(isset($payLoadData['status']) && $payLoadData['status'] =='ACTIVE'){
                       $responseData = ['status' => true, 'msaasge' => 'Success','articleStatus'=>'active'];
                        $msId   = $payLoadData['msid'];
                        $hostId = $payLoadData['parents'][0]['hostId'];
                        
                        $denmarkLiveArticleUrl = "https://timesofindia.indiatimes.com/getseourl?msid=".$msId."&hostid=".$hostId."&upcache=2";
                        $responseData = App\Helpers\Utility::doCurl("GET", $denmarkLiveArticleUrl, []);
                        //echo '<pre>';print_r($responseData);die;
                        \Log::info("responseData Data from  getseourl $denmarkLiveArticleUrl :  " . json_encode($responseData));
                        if(isset($responseData['url']) && !empty($responseData['url'])){
                           $UASObj = new App\models\UserAssignmentSubmission();
                           //$msId =5749649;
                           $liveUrlData = ['msid'=>$msId,'host_id'=>$hostId,'live_url'=>$responseData['url']];
                           $userAssignmentData= $UASObj->getUserAssignmentDataByMSID($msId);
                           //echo '<pre>';print_r($userAssignmentData);die;
                           if(isset($userAssignmentData->id)){
                               $userAssignmentData->status ='Published';
                               $userAssignmentData->live_url_data = json_encode($liveUrlData);
                               if($userAssignmentData->save()){ 
                                   $editorName = "Editor";
                                   $done_by_user_id =0;
                                    if(isset($payLoadData['updatedBy']) && !empty($payLoadData['updatedBy'])){
                                        $editorEmail   = $payLoadData['updatedBy'];
                                        $editorName = str_replace('@timesinternet.in', '', $editorEmail);
                                        $editorData =User::getLmsUserByEmail($payLoadData['updatedBy']);
                                        if(isset($editorData->id)){
                                            $editorName = $editorData->name;
                                            $done_by_user_id = $editorData->id;
                                        }
                                    }
                                    
                                    $logger['subject_id'] = $userAssignmentData->assignment_id;
                                    $logger['subject_name'] = 'Assignment';
                                    $logger['done_by_user_id'] = $done_by_user_id;
                                    $logger['activity'] = $editorName.' Published Assignment ID - '.$userAssignmentData->assignment_id.' on Denmark';
                                    \App\models\EditorLog::logEditor($logger);
                                    
                                    $assignment_id   = $userAssignmentData->assignment_id;
                                    $user_id         = $userAssignmentData->user_id;
                                    $url             = $responseData['url'];
                                    \Log::info("mailAssignmentPublishedDenmark call :  ");
                                    $UASObj->mailAssignmentPublishedDenmark($assignment_id,$user_id,$url);
                               }
                               
                           }
                        }
                    }else{
                        $responseData = ['status' => true, 'msaasge' => 'Success','articleStatus'=>'inactive'];
                    }
                    
                    //print_r($payLoadData);
                    
                } else {
                    $responseData =['status' => false, 'msaasge' => 'invalid Json'];
                }
            } else {
                $responseData = ['status' => false, 'msaasge' => 'empty post json data'];                
            }
        } catch (\Exception $ex) {
            \Log::info("Exception :  " . $ex->getMessage());
            $responseData = ['status' => false, 'msaasge' => "Exception : ".$ex->getMessage()];
        }
         \Log::info("\n-------------articlePublish Callback end-------------\n");
         return  response()->json(['response' => $responseData]);
        
                
    }
    
    
    /**
     * Upload image form for admin  for testing
     * @param Request $request
     * @return type
     */
    public function uploadImageForm(Request $request){
        return view('author.imageupload',compact('authors','languages'));        
    }
    
    /**
     * Upload image from admin for testing
     * @param Request $request
     * @return type
     */
    public function uploadImage(Request $request){
        //echo 'HH';
        if (($request->file('uploadimage'))) {
          //  echo "<pre>";
        //echo $request->email;
        // echo $request->doc_type;
        //print_r($request->file('uploadimage'));die;
            $imageUploadPath = '/userDoc/' . date('Y') . '/' . date('M') . '/';
           $adminUserData = Auth::user();
           $user_id = $adminUserData->id;
            $uploadedDocName = $request->doc_type;
            $pancard_image = $request->file('uploadimage');
            $slug = str_slug($user_id)."-admin" . '-'.$uploadedDocName;
            $currentDate = Carbon::now()->toDateString();
            $fileExtension = $pancard_image->getClientOriginalExtension();
            $imageName = $slug . '-' . $currentDate . '-' . uniqid() . '.' . $fileExtension;
            $userPayment = new \App\models\UserPaymentDetail();
            $isImgUpload = $userPayment->uploadFiles($imageName, $pancard_image, $imageUploadPath, $userPayment->pancard_image);
///userDoc/2019/Dec/75-admin--PanCard-2019-12-11-5df0a739bfa6b.jpg
            if ($isImgUpload) {
                
                $logger['subject_id'] = $user_id;
                $logger['subject_name'] = $imageUploadPath . $imageName;
                $logger['done_by_user_id'] = $adminUserData->id;
                $logger['activity'] ="User  Uploaded wrong file ".$uploadedDocName.", so that admin upload correct doc";
                \Log::info('Success : User  Uploaded wrong file : ' . json_encode($logger));
                return back()->with('success',$imageUploadPath . $imageName); 
                //echo  $imageUploadPath . $imageName;
            } else {
                return back()->with('error','Error occured while upload pancard image!');
                
            }
        }else{
            
        }
    }
    
    
    public function registrationProcessDemo(Request $request){
        $fileName ='';
        $fileType ='';
        try {
            if ($request->asset_type) {
                if ($request->asset_type == 'fisean') { // video
                    $fileType = 'Video';
                    if ($request->asset_name && $request->asset_name == 'demo-1') {
                        $fileName = "LIPI-demo-1.webm";
                    } else if ($request->asset_name && $request->asset_name == 'demo-2') {
                        $fileName = "LIPI-demo-2.webm";
                    } else if ($request->asset_name && $request->asset_name == 'demo-3') {
                        $fileName = "LIPI-demo-3.webm";
                    }
                } else if ($request->asset_type == 'iomha') { //image
                    $fileType = 'Image';
                    if ($request->asset_name && $request->asset_name == 'demo-4') {
                        $fileName = "LIPI-demo-4.png";
                    } else if ($request->asset_name && $request->asset_name == 'demo-5') {
                        $fileName = "LIPI-demo-5.png";
                    }
                }
            }
            if ($fileName) {                 
                 return view('registration-demo',compact('fileName','fileType'));                
            }else{
                return [ 'status' => false, 'message' => 'File not exist'];
                 
            }
            return [ 'status' => false, 'message' => 'File not exist'];
            
        } catch (\Exception $ex) {
            \Log::info('Exception on ' . __FUNCTION__ . " : " . json_encode($ex->getMessage()));
            return false;
        }
    }

}